// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#pragma once

#include "MediaTrackModel.h"

class VideoTrackModel : public MediaTrackModel<libvlc_track_video>
{
public:
    using MediaTrackModel::MediaTrackModel;
};
