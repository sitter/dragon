// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#pragma once

#include <vlc/vlc.h>

#include <QObject>

#include "LibVLC.h"

class Dialog : public QObject
{
    Q_OBJECT
public:
    enum class QuestionType {
        Normal = LIBVLC_DIALOG_QUESTION_NORMAL,
        Warning = LIBVLC_DIALOG_QUESTION_WARNING,
        Critical = LIBVLC_DIALOG_QUESTION_CRITICAL,
    };
    Q_ENUM(QuestionType);

    explicit Dialog(LibVLC *vlc = LibVLC::instance());

    Q_SIGNAL void
    displayLogin(intptr_t id, const QString &title, const QString &text, const QString &defaultUser, bool askStore);
    Q_SIGNAL void displayQuestion(intptr_t id, const QString &title, const QString &text, Dialog::QuestionType type, const QString &cancel, const QString &action1, const QString &action2);

    Q_SIGNAL void error(const QString &title, const QString &text);
    Q_SIGNAL void cancel(intptr_t id);

    Q_INVOKABLE void login(intptr_t id, const QString &user, const QString &password, bool store);
    Q_INVOKABLE void action() {}
    Q_INVOKABLE void dismiss(intptr_t id);
};

namespace DialogCaller
{
void pf_display_login(void *p_data,
                      libvlc_dialog_id *p_id,
                      const char *psz_title,
                      const char *psz_text,
                      const char *psz_default_username,
                      bool b_ask_store);
void pf_display_question(void *p_data,
                         libvlc_dialog_id *p_id,
                         const char *psz_title,
                         const char *psz_text,
                         libvlc_dialog_question_type i_type,
                         const char *psz_cancel,
                         const char *psz_action1,
                         const char *psz_action2);
void pf_display_progress(void *p_data,
                         libvlc_dialog_id *p_id,
                         const char *psz_title,
                         const char *psz_text,
                         bool b_indeterminate,
                         float f_position,
                         const char *psz_cancel);
void pf_cancel(void *p_data, libvlc_dialog_id *p_id);
void pf_update_progress(void *p_data, libvlc_dialog_id *p_id, float f_position, const char *psz_text);
void error(void *p_data, const char *psz_title, const char *psz_text);
} // namespace DialogCaller
