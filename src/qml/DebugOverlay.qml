// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2021-2022 Harald Sitter <sitter@kde.org>

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as QQC2
import org.kde.kirigami 2.19 as Kirigami
import QtQuick.Window 2.15 as Window
import org.kde.coreaddons 1.0 as KCoreAddons
import Qt.labs.animation 1.0
import Qt.labs.platform 1.1 as LabsPlatform
import QtQml.Models 2.15 as Models

import org.kde.dragon 1.0

// TODO divide this up a bit. the file is getting really long

ColumnLayout {
    id: root

    required property var player

    anchors.fill: parent

    RowLayout {
        QQC2.Label {
            text: root.player.volume
            color: "white"
        }
    }

    RowLayout {
        Layout.fillHeight: true
        Layout.fillWidth: true
        ListView {
            width: 200
            Layout.fillHeight: true
            model: MediaStatistics {
                media: root.player.media
            }
            delegate: QQC2.Label {
                Layout.fillWidth: true
                text: "%1: %2".arg(ROLE_Key).arg(ROLE_Value)
            }
            interactive: false
        }

        ListView {
            Layout.fillHeight: true
            width: 200
            model: SubtitleTrackModel {
                player: root.player
            }
            delegate: RowLayout {
                QQC2.Label {
                    text: ROLE_Selected ? "[x]" : "[ ]"
                    color: "red"
                }
                QQC2.Label {
                    text: display
                    color: "red"
                }
            }
            interactive: false
        }

        ListView {
            Layout.fillHeight: true
            width: 200
            model: VideoTrackModel {
                player: root.player
            }
            delegate: RowLayout{
                QQC2.Label {
                    text: ROLE_Selected ? "[x]" : "[ ]"
                    color: "lightgreen"
                }
                QQC2.Label {
                    text: display
                    color: "lightgreen"
                }
            }
            interactive: false
        }

        ListView {
            Layout.fillHeight: true
            width: 200
            model: AudioTrackModel {
                player: root.player
            }
            delegate: RowLayout {
                QQC2.Label {
                    text: ROLE_Selected ? "[x]" : "[ ]"
                    color: "lightblue"
                }
                QQC2.Label {
                    text: display
                    color: "lightblue"
                }
            }
            interactive: false
        }

        ListView {
            Layout.fillHeight: true
            width: 200
            model: root.player.list
            delegate: QQC2.Label {
                text: display
                color: "pink"
            }
            interactive: false
        }
    }
}
