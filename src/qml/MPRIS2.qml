// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2021-2022 Harald Sitter <sitter@kde.org>

import QtQuick 2.15
import org.kde.dragon 1.0

Bus {
    id: bus

    required property var player
    required property var videoContainer

    BusService {
        name: "org.mpris.MediaPlayer2.dragonplayer"

        BusObject {
            path: "/org/mpris/MediaPlayer2"

            // NOTE: property writing needs to be implemented by implementing the appropriate setFoo function -
            //   bindings must never be broken implicitly here!

            MediaPlayer2 {
                property bool canQuit: true
                property bool canRaise: true // TODO only when not raised surely
                property bool canSetFullscreen: true // TODO are we constrained on anything here?
                readonly property string desktopEntry: "org.kde.dragonplayer"
                property bool fullscreen: videoContainer.state.startsWith("fullscreen")
                property bool hasTrackList: false // TODO not true is it?
                property string identity: i18nc("@title MPRIS2 application name", "Dragon Player")
                readonly property var supportedMimeTypes: Context.mimetypes
                readonly property var supportedUriSchemes: Context.protocols

                function setFullscreen(fullscreen) {
                    if (fullscreen) {
                        videoPage.fullscreen()
                    } else {
                        videoPage.cancelFullscreen()
                    }
                }

                function quit() {
                    Qt.quit()
                }

                function raise() {
                    appWindow.raise()
                }
            }

            MediaPlayer2Player {
                property string playbackStatus: {
                    if (player.paused) {
                        return "Paused"
                    }
                    if (player.stopped) {
                        return "Stopped"
                    }
                    return "Playing"
                }
                property string loopStatus: "None"
                property double rate: 1.0
                property bool shuffle: false
                property var metadata // TODO set
                property double volume: player.volume / 100
                property int position: player.time
                property double minimumRate: 1.0
                property double maximumRate: 1.0
                property bool canGoNext: false
                property bool canGoPrevious: false
                property bool canPlay: true
                property bool canPause: player.pausable
                property bool canSeek: player.seekable
                property bool canControl: true

                signal seeked(int position)

                function setRate(rate) { } // noop

                function setVolume(volume) {
                    player.volume = volume * 100
                }

                function setLoopStatus(status) {
                    console.warning("setLoopStatus not supported")
                }

                function setShuffle(shuffle) {
                    player.shuffle = shuffle
                }

                function next() {
                    console.warning("next() not supported")
                }

                function previous() {
                    console.warning("previous() not supported")
                }

                function pause() {
                    if (player.pausable) {
                        player.togglePause()
                    }
                }

                function playPause() {
                    player.togglePause()
                }

                function stop() {
                    player.stop()
                }

                function play() {
                    player.play()
                }

                function seek(offset) {
                    player.seek(offset, true)
                    seeked(offset) // VLC doesn't emit a seeked signal so we don't actually know when the seek has been executed. fake it instead.
                }

                function openUri(uri) {
                    player.play(uri)
                }
            }
        }
    }
}
