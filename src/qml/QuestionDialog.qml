// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2021-2022 Harald Sitter <sitter@kde.org>

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as QQC2
import org.kde.kirigami 2.19 as Kirigami
import QtQuick.Window 2.15 as Window
import QtGraphicalEffects 1.15
import org.kde.coreaddons 1.0 as KCoreAddons
import Qt.labs.animation 1.0
import QtQuick.Dialogs 1.1 as Dialogs

import org.kde.dragon 1.0

QQC2.Dialog {
    id: messageDialog

    property var type
    required property Dialog dialogContext
    modal: true


    // TODO

    Component.onCompleted: console.log(type)
}



// Dialogs.Dialog {
//     id: control

//     property string title

//     header: Pane {
//         palette.window: control.palette.light
//         padding: 20

//         contentItem: Label {
//             width: parent.width
//             text: control.title
//             visible: control.title.length > 0
//             horizontalAlignment: Label.AlignHCenter
//             elide: Label.ElideRight
//             font.bold: true
//         }
//     }

//     contentItem: Column {
//         padding: 10
//         spacing: 16

//         Label {
//             id: textLabel
//             objectName: "textLabel"
//             text: control.text
//             visible: text.length > 0
//             wrapMode: Text.Wrap
//             width: parent.width - parent.leftPadding - parent.rightPadding

//         }

//         Label {
//             id: informativeTextLabel
//             objectName: "informativeTextLabel"
//             text: control.informativeText
//             visible: text.length > 0
//             wrapMode: Text.Wrap
//             width: parent.width - parent.leftPadding - parent.rightPadding
//         }
//     }

//     footer: ColumnLayout {
//         id: columnLayout

//         RowLayout {
//             id: rowLayout
//             spacing: 12

//             Layout.leftMargin: 20
//             Layout.rightMargin: 20
//             Layout.bottomMargin: 20

//             Button {
//                 id: detailedTextButton
//                 objectName: "detailedTextButton"
//                 text: control.showDetailedText ? qsTr("Hide Details...") : qsTr("Show Details...")
//                 padding: 0
//             }

//             DialogButtonBox {
//                 id: buttonBox
//                 objectName: "buttonBox"
//                 spacing: 12
//                 padding: 0

//                 Layout.fillWidth: true
//             }
//         }

//         TextArea {
//             id: detailedTextArea
//             objectName: "detailedText"
//             text: control.detailedText
//             visible: control.showDetailedText
//             wrapMode: TextEdit.WordWrap
//             readOnly: true

//             Layout.fillWidth: true
//             Layout.leftMargin: 20
//             Layout.rightMargin: 20
//             Layout.bottomMargin: 20

//             background: Rectangle {
//                 color: Qt.rgba(1,1,1,1)
//                 radius: 3
//                 border.color: Qt.darker(control.palette.light)
//                 border.width: 1
//             }
//         }
//     }

//     Overlay.modal: Rectangle {
//         color: Color.transparent(control.palette.shadow, 0.5)
//     }

//     Overlay.modeless: Rectangle {
//         color: Color.transparent(control.palette.shadow, 0.12)
//     }
// }
