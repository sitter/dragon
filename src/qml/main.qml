// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2021-2022 Harald Sitter <sitter@kde.org>

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as QQC2
import org.kde.kirigami 2.19 as Kirigami
import QtQuick.Window 2.15 as Window
import org.kde.coreaddons 1.0 as KCoreAddons
import Qt.labs.animation 1.0
import Qt.labs.platform 1.1 as LabsPlatform
import QtQml.Models 2.15 as Models

import org.kde.dragon 1.0

Kirigami.ApplicationWindow {
    id: appWindow

    minimumWidth: Kirigami.Settings.isMobile ? 0 : Kirigami.Units.gridUnit * 22
    minimumHeight: Kirigami.Settings.isMobile ? 0 : Kirigami.Units.gridUnit * 22

    LabsPlatform.FileDialog {
        id: fileDialog
        folder: LabsPlatform.StandardPaths.writableLocation(LabsPlatform.StandardPaths.MoviesLocation)
        onFileChanged: playerPage.player.play(file)
    }

    MPRIS2 {
        player: playerPage.player
        videoContainer: playerPage.videoContainer
    }

    PlayerPage {
        id: playerPage
    }

    pageStack.initialPage: playerPage

    // FIXME this is mighty buggered when the file name contains fragment characters and the like
    Component.onCompleted: playerPage.player.play(Application.arguments[1])
}
