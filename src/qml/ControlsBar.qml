// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2021-2022 Harald Sitter <sitter@kde.org>

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as QQC2
import org.kde.kirigami 2.19 as Kirigami
import QtQuick.Window 2.15 as Window
import org.kde.coreaddons 1.0 as KCoreAddons
import Qt.labs.animation 1.0
import Qt.labs.platform 1.1 as LabsPlatform
import QtQml.Models 2.15 as Models

import org.kde.dragon 1.0

QQC2.ToolBar {
    id: toolbar
    readonly property var hiddenInset: -contentHeight
    property alias volumeButton: volumeButton
    property alias toolbarHandler: toolbarHandler
    required property var player

    visible: topInset !== hiddenInset

    Behavior on topInset {
        NumberAnimation { duration: Kirigami.Units.veryShortDuration }
    }

    Behavior on topPadding {
        NumberAnimation { duration: Kirigami.Units.veryShortDuration }
    }

    RowLayout {
        anchors.fill: parent

        QQC2.ToolButton {
            display: QQC2.AbstractButton.IconOnly
            action: Kirigami.Action {
                text: "Open..."
                icon.name: "document-open"
                onTriggered: fileDialog.open()
            }
        }

        Kirigami.Separator {
            Layout.fillHeight: true
            width: 1
        }

        QQC2.ToolButton {
            display: QQC2.AbstractButton.IconOnly
            action: togglePauseAction
        }

        QQC2.Slider {
            id: seekSlider

            Layout.fillWidth: true

            anchors.left: video.left
            anchors.right: video.right
            anchors.bottom: video.bottom
            hoverEnabled: false
            from: 0
            to: player.length
            onMoved: videoPage.seek(value, true)
            wheelEnabled: true
            enabled: !player.stopped

            Behavior on opacity {
                NumberAnimation { duration: Kirigami.Units.shortDuration }
            }

            Binding {
                target: seekSlider
                property: 'value'
                value: player.time
                when: !seekSlider.pressed
                delayed: true
            }
        }

        QQC2.ToolButton {
            display: QQC2.AbstractButton.IconOnly
            action: fullscreenAction
        }

        QQC2.ToolButton {
            id: volumeButton
            display: QQC2.AbstractButton.IconOnly
            action: Kirigami.Action {
                text: i18nc("@action:button", "Volume")
                icon.name: "player-volume"
                onTriggered: volumeButton.popup.open()
            }
            readonly property var popup : QQC2.Popup {
                topMargin: volumeButton.height
                QQC2.Slider {
                    orientation: Qt.Vertical
                    from: 0
                    to: 100
                    value: player.volume
                    wheelEnabled: false
                    onMoved: player.volume = value
                }
            }
        }

        QQC2.ToolButton {
            id: menuButton
            readonly property var menu: QQC2.Menu {
                topMargin: menuButton.height

                Kirigami.Action {
                    text: i18nc("@action:button", "Stop")
                    enabled: !player.stopped
                    icon.name: "media-playback-stop"
                    onTriggered: player.stopped = true
                    shortcut: "S"
                }

                Kirigami.Action {
                    text: player.muted ? i18nc("@action:button", "Unmute") : i18nc("@action:button", "Mute")
                    icon.name: player.muted ? "player-volume" : "player-volume-muted"
                    onTriggered: player.muted = !player.muted
                    shortcut: "M"
                }

                component MediaTrackRadioButton : QQC2.RadioButton {
                    text: model.display
                    checked: ROLE_Selected
                    onToggled: ROLE_Selected = checked
                }

                QQC2.Menu {
                    title: "Subtitle"
                    enabled: count > 1 // always one repeater
                    Repeater {
                        model: SubtitleTrackModel { player: toolbar.player }
                        delegate: MediaTrackRadioButton {}
                    }
                }
                QQC2.Menu {
                    title: "Audio Track"
                    enabled: count > 1 // always one repeater
                    Repeater {
                        model: AudioTrackModel { player: toolbar.player }
                        delegate: MediaTrackRadioButton {}
                    }
                }
                QQC2.Menu {
                    title: "Video Track"
                    enabled: count > 1 // always one repeater
                    Repeater {
                        model: VideoTrackModel { player: toolbar.player }
                        delegate: MediaTrackRadioButton {}
                    }
                }
                Kirigami.Action {
                    id: aboutAction
                    displayHint: Kirigami.Action.AlwaysHide
                    icon.name: "dragonplayer"
                    text: i18nc("@action opens about app page", "About")
                    onTriggered: { pageStack.layers.push("qrc:/ui/AboutPage.qml") }
                }
            }
            display: QQC2.AbstractButton.IconOnly
            action: Kirigami.Action {
                text: i18nc("@action:button", "Application Menu")
                icon.name: "open-menu-symbolic"
                onTriggered: menuButton.menu.open()
            }
        }
    }

    HoverHandler {
        id: toolbarHandler
        margin: Kirigami.Units.gridUnit
        onHoveredChanged: activeTimer.restart()
    }
}
