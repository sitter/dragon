// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2021-2022 Harald Sitter <sitter@kde.org>

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as QQC2
import org.kde.kirigami 2.19 as Kirigami
import QtQuick.Window 2.15 as Window
import org.kde.coreaddons 1.0 as KCoreAddons
import Qt.labs.animation 1.0

import org.kde.dragon 1.0

Kirigami.Page {
    property var id: -1
    property alias text: textLabel.text
    property alias defaultUser: userField.placeholderText
    property bool askStore: false
    required property Dialog dialogContext

    onBackRequested: {
        dialogContext.dismiss(id)
    }

    ColumnLayout {
        anchors.centerIn: parent
        QQC2.Label {
            id: textLabel
        }
        Kirigami.FormLayout {
            QQC2.TextField {
                id: userField
                Kirigami.FormData.label: i18nc("@label", "Username:")
            }
            QQC2.TextField {
                id: passwordField
                Kirigami.FormData.label: i18nc("@label", "Password:")
                echoMode: TextInput.Password
            }
            QQC2.CheckBox {
                id: storeBox
                visible: askStore
                Kirigami.FormData.label: i18nc("@label", "Remember:")
            }
        }
        QQC2.Button {
            action: Kirigami.Action {
                text: "Login"
                onTriggered: {
                    dialogContext.login(id, userField.text, passwordField.text, storeBox.checked)
                    appWindow.pageStack.layers.pop()
                }
            }
        }
    }
}
