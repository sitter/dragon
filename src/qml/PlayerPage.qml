// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2021-2022 Harald Sitter <sitter@kde.org>

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as QQC2
import org.kde.kirigami 2.19 as Kirigami
import QtQuick.Window 2.15 as Window
import org.kde.coreaddons 1.0 as KCoreAddons
import Qt.labs.animation 1.0
import Qt.labs.platform 1.1 as LabsPlatform
import QtQml.Models 2.15 as Models

import org.kde.dragon 1.0

// TODO divide this up a bit. the file is getting really long

Kirigami.Page {
    id: videoPage

    property var storedVisibility: null
    property alias player: player
    property alias videoContainer: videoContainer

    globalToolBarStyle: pageStack.layers.currentItem === undefined ? Kirigami.ApplicationHeaderStyle.None : undefined
    leftPadding: 0
    topPadding: 0
    rightPadding: 0
    bottomPadding: 0

    Kirigami.Action {
        id: togglePauseAction
        text: player.paused || player.stopped ? i18nc("@action:button", "Play") : i18nc("@action:button", "Pause")
        icon.name: player.paused || player.stopped ? "media-playback-start" : "media-playback-pause"
        onTriggered: player.stopped ? player.play() : player.togglePause()
    }

    Kirigami.Action {
        id: fullscreenAction
        text: visibility === Window.Window.FullScreen ? i18nc("@action:button", "Cancel Fullscreen") : i18nc("@action:button", "Fullscreen")
        icon.name: "view-fullscreen"
        onTriggered: videoPage.toggleFullscreen()
        shortcut: "F"
    }

    ColumnLayout {
        id: toolbarLayout
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        z: 2 // so we are on top of the video item!

        ControlsBar {
            id: toolbar
            Layout.fillWidth: true
            player: player
        }

        Kirigami.InlineMessage {
            id: errorMessage
            Layout.fillWidth: true
            type: Kirigami.MessageType.Error
            showCloseButton: true
        }
    }

    function cancelFullscreen() {
        if (storedVisibility !== null) {
            visibility = storedVisibility
            storedVisibility = null
        }
    }

    function fullscreen() {
        if (storedVisibility === null) {
            storedVisibility = visibility
            visibility = Window.Window.FullScreen
            return true
        }
        return false
    }

    function toggleFullscreen() {
        if (fullscreen()) {
            return
        } else {
            cancelFullscreen()
        }
    }

    function seek(time, precise) {
        player.seek(time, precise)
    }

    Item {
        id: videoContainer
        anchors.top: toolbarLayout.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom

        VideoItem {
            id: video
            anchors.fill: parent
        }

        MediaPlayer {
            id: player
            videoOutput: video.output

            BoundaryRule on volume {
                id: volumeBoundaryRule
                minimum: 0
                maximum: 100
                // minimumOvershoot: 400; maximumOvershoot: 400
                overshootFilter: BoundaryRule.Peak
            }
            onVolumeChanged: volumeWheel.rotation = volume

            Component.onDestruction: stopped = true
        }

        // TODO reset all dialogs when the media changes!
        Dialog {
            id: dialogContext

            onError: {
                errorMessage.text = title
                errorMessage.visible = true
            }

            onDisplayLogin: (id, title, text, defaultUser, askStore) => {
                appWindow.pageStack.layers.push("LoginPage.qml", { id: id, title: title, text: text, defaultUser: defaultUser, askStore: askStore, dialogContext: dialogContext })
            }

            onDisplayQuestion: (id, title, text, type, cancel, action1, action2) => {
                console.log("hello")
                const component = Qt.createComponent("QuestionDialog.qml");
                console.log(component.errorString())
                console.log(component.status)
                // if (component.status == Component.Ready) {
                    const obj = component.createObject(null, { id: id, title: title, text: text, type: type, dialogContext: dialogContext });
                    console.log(obj)
                    obj.visible = true
                // }

            }

            onCancel: {
                console.log(appWindow.pageStack.layers)
                const page = appWindow.pageStack.layers.find(function(item, index) {
                    return item.id == id
                })
                let lastItem = undefined
                do {
                    lastItem = appWindow.pageStack.layers.pop()
                } while (lastItem !== page && !appWindow.pageStack.layers.empty)
            }

        }

        DebugOverlay {
            player: player
        }

        WheelHandler {
            id: volumeWheel
            onWheel: player.volume = rotation
            onActiveChanged: {
                if (!active) {
                    rotation = player.volume
                    volumeBoundaryRule.returnToBounds()
                }
            }
        }

        TapHandler {
            acceptedButtons: Qt.AllButtons
            onDoubleTapped: videoPage.toggleFullscreen()
        }

        MouseArea {
            id: mainHoverHandler

            property var initialPoint: null
            readonly property var resetTimer: Timer {
                interval: Kirigami.Units.veryShortDuration
                repeat: false
                running: false
                onTriggered: parent.initialPoint = null
            }

            anchors.fill: parent
            acceptedButtons: Qt.NoButton
            hoverEnabled: true
            propagateComposedEvents: true
            onPositionChanged: (event) => {
                const position = Qt.point(event.x, event.y)
                if (initialPoint === null) {
                    initialPoint = position
                    resetTimer.restart()
                    return
                } else {
                    const distance = Math.sqrt((initialPoint.x - position.x) ** 2 + (initialPoint.y - position.y) ** 2);
                    if (distance > Kirigami.Units.gridUnit) { // FIXME this should somehow relate to window size
                        activeTimer.restart()
                    }
                }
                resetTimer.restart()
            }
        }

        Rectangle {
            id: volumeDisplay

            property var volume: player.volume
            property var hideTimer: Timer {
                interval: 2000
                repeat: false
                onTriggered: volumeDisplay.opacity = 0
            }

            visible: opacity > 0
            opacity: 0
            implicitWidth: Kirigami.Units.gridUnit
            implicitHeight: video.height / 1.5
            border.color: "white" // TODO
            border.width: 2
            anchors.right: video.right
            anchors.rightMargin: Kirigami.Units.gridUnit * 2
            y: video.height / 2 - height / 2
            color: "transparent"

            onVolumeChanged: {
                if (toolbar.volumeButton.popup.visible) {
                    return
                }
                opacity = 1
                volumeDisplay.hideTimer.restart()
            }

            Behavior on opacity {
                NumberAnimation { duration: Kirigami.Units.shortDuration }
            }

            Rectangle {
                width: parent.width - (parent.border.width * 2)
                height: parent.height * (player.volume / 100) - (parent.border.width * 2)
                color: "red" // TODO
                anchors.left: parent.left
                anchors.leftMargin: 2
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 2
            }
        }

        Item {
            id: timeItem

            anchors.fill: video
            visible: opacity > 0
            opacity: toolbar.toolbarHandler.hovered ? 1 : 0
            Behavior on opacity {
                NumberAnimation { duration: Kirigami.Units.shortDuration }
            }

            Rectangle {
                anchors.centerIn: parent
                color: "black" // TODO
                implicitWidth: childrenRect.width
                implicitHeight: childrenRect.height
                Kirigami.Heading {
                    anchors.centerIn: parent
                    font.pixelSize: 128 / 2 // FIXME
                    color: "white" // TODO
                    text: "%1 / %2".arg(KCoreAddons.Format.formatDuration(player.time)).arg(KCoreAddons.Format.formatDuration(player.length))
                }
            }
        }

        Timer {
            id: activeTimer
            interval: 2000
            repeat: false
        }

        states: [
            State {
                name: "fullscreen-active"
                extend: "fullscreen"
                when: appWindow.visibility === Window.Window.FullScreen && (activeTimer.running || toolbar.toolbarHandler.hovered)
                PropertyChanges {
                    target: toolbar
                    topInset: { toolbar.topInset = 0 }
                    topPadding: { toolbar.topPadding = 0 }
                }
            },
            State {
                name: "fullscreen"
                when: appWindow.visibility === Window.Window.FullScreen
                PropertyChanges {
                    target: toolbar
                    topInset: { toolbar.topInset = toolbar.hiddenInset }
                    topPadding: { toolbar.topPadding = toolbar.hiddenInset }
                }
                PropertyChanges {
                    target: videoContainer
                    anchors.top: parent.top
                }
            },
            State {
                name: "" // default state
            }
        ]
        onStateChanged: {
            console.log("state", state)
        }
    }

    FPSItem {
        id: fpsItem
        anchors.centerIn: parent
    }

    Shortcut {
        // FIXME: broken shambles mediaplay doesn't actually work at all, it is the correct entity though
        sequences: ["Space", Qt.Key_MediaPlay]
        onActivated: togglePauseAction.trigger()
    }

    Shortcut {
        sequence: StandardKey.Cancel
        onActivated: videoPage.cancelFullscreen()
    }

    Shortcut {
        sequence: "Left"
        onActivated: videoPage.seek(seekSlider.value - 5000, true)
    }

    Shortcut {
        sequence: "Right"
        onActivated: videoPage.seek(seekSlider.value + 5000, true)
    }

    Shortcut {
        sequence: "Ctrl+Left"
        onActivated: videoPage.seek(seekSlider.value - 60000, true)
    }

    Shortcut {
        sequence: "Ctrl+Right"
        onActivated: videoPage.seek(seekSlider.value + 60000, true)
    }

    Shortcut {
        sequence: "Ctrl+Alt+Left"
        onActivated: videoPage.seek(seekSlider.value - (5 * 60000), true)
    }

    Shortcut {
        sequence: "Ctrl+Alt+Right"
        onActivated: videoPage.seek(seekSlider.value + (5 * 60000), true)
    }
}
