// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#include "Media.h"

#include <chrono>

#include <QDebug>
#include <QEventLoop>
#include <QTimer>
#include <QUrl>

// #include <KIO/FileJob>
// #include <KIO/StatJob>
// #include <KProtocolManager>

// Todo
#include <QCoreApplication>
#include <QThread>

using namespace std::chrono_literals;

// class KIOInterface : public QObject
// {
//     Q_OBJECT
// public:
//     explicit KIOInterface(const QUrl &mrl, QObject *parent = nullptr)
//         : QObject(parent)
//         , m_job(KIO::open(mrl, QIODevice::ReadOnly))
//     {
//         QEventLoop loop;
//         connect(m_job, &KIO::FileJob::open, &loop, [this, &loop]() {
//             m_isOpen = true;
//             loop.quit();
//         });
//         connect(m_job, &KJob::result, &loop, [this, &loop] {
//             if (m_job->error() != KJob::NoError) {
//                 qWarning() << m_job->error() << m_job->errorString() << m_job->errorString();
//             }
//             loop.quit();
//         });
//         QTimer::singleShot(30s, &loop, &QEventLoop::quit);
//         m_job->start();
//         loop.exec(QEventLoop::ExcludeUserInputEvents);
//     }

//     ptrdiff_t read(unsigned char *buf, size_t len)
//     {
//         QEventLoop loop;
//         ptrdiff_t returnValue = -1;
//         Q_ASSERT(std::numeric_limits<ptrdiff_t>::max() >= len); // we later cast size_t into the return value
//         constexpr auto maxChunkSize = 14 * 1024 * 1024;
//         const auto blobSize = qMin<size_t>(len, maxChunkSize);
//         connect(m_job,
//                 &KIO::FileJob::data,
//                 &loop,
//                 [&loop, &buf, blobSize, &returnValue](KIO::Job *, const QByteArray &data) {
//                     const auto realSize = qMin<size_t>(blobSize, data.size());
//                     memcpy(buf, data.constData(), realSize);
//                     returnValue = ptrdiff_t(realSize);
//                     loop.quit();
//                 });
//         connect(m_job, &KJob::result, &loop, [this, &returnValue] {
//             if (m_job->error() != KJob::NoError) {
//                 qWarning() << m_job->error() << m_job->errorString() << m_job->errorString();
//                 returnValue = -1;
//                 return;
//             }
//             if (returnValue < 0) {
//                 returnValue = 0; // end of data (if returnValue is >0 we have still read data and need one more read cycle to reach EOD)
//             }
//         });
//         m_job->read(blobSize);
//         loop.exec(QEventLoop::ExcludeUserInputEvents);
//         return returnValue;
//     }

//     int seek(uint64_t offset)
//     {
//         QEventLoop loop;
//         int returnValue = -1;
//         connect(m_job, &KIO::FileJob::position, &loop, [&loop, &returnValue](KIO::Job *, KIO::filesize_t) {
//             returnValue = 0;
//             loop.quit();
//         });
//         connect(m_job, &KJob::result, &loop, [this] {
//             qWarning() << "Unexpected job exit" << m_job->error() << m_job->errorString() << m_job->errorString();
//         });
//         m_job->seek(offset);
//         loop.exec(QEventLoop::ExcludeUserInputEvents);
//         return returnValue;
//     }

//     void close()
//     {
//         m_job->close();
//         deleteLater();
//     }

//     KIO::filesize_t size() const
//     {
//         return m_job->size();
//     }

//     bool m_isOpen = false;
//     KIO::FileJob * const m_job;
// };

class KIOInterfaceFactory : public QObject
{
public:
    explicit KIOInterfaceFactory(const QUrl &mrl, QObject *parent = nullptr)
        : QObject(parent)
        , m_mrl(mrl)
    {
        qDebug() << this << m_mrl;
        #warning hack to get auth to work, kind of
        // KIO::stat(mrl);
    }

    int open(void **datap, uint64_t *sizep)
    {
        // qDebug() << this << m_mrl;
        // KIOInterface *iface = nullptr;
        // // Create the interface on the gui thread. KIO doesn't like living on foreign (non-QThreads) because of timer
        // requirements QMetaObject::invokeMethod(
        //     qApp, [&iface, this] { iface = new KIOInterface(m_mrl); }, Qt::BlockingQueuedConnection);
        // Q_ASSERT(iface);
        // *datap = iface;
        // *sizep = iface->size() > 0 ? iface->size() : std::numeric_limits<quint64>::max();
        // return iface->m_isOpen ? 0 : -1;
        return -1;
    }

private:
    const QUrl m_mrl;
};

Media::Media(const QUrl &mrl, QObject *parent)
    // : m_media(mrl.scheme() != "file" && KProtocolManager::supportsOpening(mrl)
    //               ? libvlc_media_new_callbacks(IOCaller::open,
    //                                            IOCaller::read,
    //                                            IOCaller::seek,
    //                                            IOCaller::close,
    //                                            new KIOInterfaceFactory(mrl, this))
    //               : libvlc_media_new_location(qUtf8Printable(mrl.toString())))
    : Media(libvlc_media_new_location(qUtf8Printable(mrl.toString())), Retain::No /* already refcounted, presumably */, parent)
{
}

Media::Media(libvlc_media_t *media, Retain retain, QObject *parent)
    : QObject(parent)
    , m_media(media)
{
    qDebug() << "gui thread" << QThread::currentThread();
    if (retain == Retain::Yes) {
        libvlc_media_retain(get()); // will get released when the unique_ptr gets destroyed
    }
}

int IOCaller::open(void *opaque, void **datap, uint64_t *sizep)
{
    qDebug() << Q_FUNC_INFO;
    return static_cast<KIOInterfaceFactory*>(opaque)->open(datap, sizep);
}

ptrdiff_t IOCaller::read(void *opaque, unsigned char *buf, size_t len)
{
    ptrdiff_t ret = -1;
    // auto that = static_cast<KIOInterface *>(opaque);
    // QMetaObject::invokeMethod(
    //     that, [that, buf, len, &ret] { ret = that->read(buf, len); }, Qt::BlockingQueuedConnection);

    return ret;
}

int IOCaller::seek(void *opaque, uint64_t offset)
{
    qDebug() << Q_FUNC_INFO;
    int ret = -1;
    // auto that = static_cast<KIOInterface *>(opaque);
    // QMetaObject::invokeMethod(
    //     that, [that, offset, &ret] { ret = that->seek(offset); }, Qt::BlockingQueuedConnection);
    return ret;
}

void IOCaller::close(void *opaque)
{
    qDebug() << Q_FUNC_INFO;
    // auto that = static_cast<KIOInterface *>(opaque);
    // QMetaObject::invokeMethod(
    //     that, [that] { that->close(); }, Qt::QueuedConnection);
}

QUrl Media::mrl() const
{
    return QUrl(QString::fromUtf8(libvlc_media_get_mrl(get())));
}

#include "Media.moc"
