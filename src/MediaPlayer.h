// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#pragma once

#include <vlc/vlc.h>

#include <memory>

#include <QObject>

#include "LibVLC.h"
#include "Media.h"
#include "VideoOutput.h"

namespace std
{
template<>
struct default_delete<libvlc_media_player_t> {
    void operator()(libvlc_media_player_t *ptr) const
    {
        libvlc_media_player_release(ptr);
    }
};
} // namespace std


class MediaPlayer : public QObject
{
    Q_OBJECT
    Q_PROPERTY(qint64 time MEMBER m_time NOTIFY timeChanged)
    Q_PROPERTY(qint64 length MEMBER m_length NOTIFY lengthChanged)
    Q_PROPERTY(int volume MEMBER m_volume WRITE setVolume NOTIFY volumeChanged)
    Q_PROPERTY(bool seekable MEMBER m_seekable NOTIFY seekableChanged)
    Q_PROPERTY(VideoOutput *videoOutput WRITE setVideoOutput NOTIFY videoOutputChanged REQUIRED)
    Q_PROPERTY(bool paused MEMBER m_paused NOTIFY pausedChanged)
    Q_PROPERTY(bool pausable MEMBER m_pausable NOTIFY pausableChanged)
    Q_PROPERTY(bool muted MEMBER m_muted WRITE setMuted NOTIFY mutedChanged)
    Q_PROPERTY(bool stopped MEMBER m_stopped WRITE setStop NOTIFY stoppedChanged)
    Q_PROPERTY(Media *media READ media NOTIFY mediaChanged)

    friend class MediaPlayer2PlayerAdaptor;

public:
    explicit MediaPlayer(LibVLC *vlc = LibVLC::instance(), QObject *parent = nullptr);
    libvlc_media_player_t *get() { return m_player.get(); }

    Q_INVOKABLE bool play();
    Q_INVOKABLE bool play(const QString &path);
    Q_INVOKABLE bool play(const QUrl &url);
    virtual bool play(std::unique_ptr<Media> media);
    Q_INVOKABLE void togglePause();
    Q_INVOKABLE void seek(qint64 time, bool fast);
    Q_INVOKABLE void setVideoOutput(VideoOutput *videoOutput);
    Q_INVOKABLE void setVolume(int volume);
    Q_INVOKABLE void setMuted(bool mute);
    Q_INVOKABLE void setStop(bool stop = true);
    Q_INVOKABLE Media *media();

    Q_SIGNAL void timeChanged();
    Q_SIGNAL void lengthChanged();
    Q_SIGNAL void volumeChanged();
    Q_SIGNAL void seekableChanged();
    Q_SIGNAL void videoOutputChanged();
    Q_SIGNAL void pausedChanged();
    Q_SIGNAL void pausableChanged();
    Q_SIGNAL void mutedChanged();
    Q_SIGNAL void stoppedChanged();
    Q_SIGNAL void mediaChanged();

    Q_SIGNAL void tracksChanged();

protected:
    std::unique_ptr<libvlc_media_player_t> m_player;
    std::unique_ptr<Media> m_media;

private:
    static void event_cb(const libvlc_event_t *event, void *opaque);

    qint64 m_time = -1;
    qint64 m_length = -1;
    int m_volume = -1;
    bool m_seekable = false;
    bool m_paused = true;
    bool m_pausable = false;
    bool m_muted = false;
    bool m_stopped = false;
};

