// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2021 Harald Sitter <sitter@kde.org>

#include <cctype>
#include <filesystem>

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQmlEngine>
#include <QSurfaceFormat>

#include <KAboutData>
#include <KLocalizedContext>
#include <KLocalizedString>

#include "dragon.h"

using namespace Qt::StringLiterals;

int main(int argc, char **argv)
{
    // FIXME surely we could just create the context in the right thread?
    QCoreApplication::setAttribute(Qt::AA_DontCheckOpenGLContextThreadAffinity);
    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);

    QApplication app(argc, argv);

    KAboutData aboutData(u"dragonplayer"_s,
                         i18n("Dragon Player"),
                         Dragon::version,
                         i18n("A video player that has a usability focus"),
                         KAboutLicense::GPL_V2,
                         i18n("Copyright 2006, Max Howell\nCopyright 2007, Ian Monroe\nCopyright 2022 Harald Sitter"),
                         QString() /* otherText */,
                         u"https://commits.kde.org/dragon"_s);
    aboutData.setDesktopFileName(Dragon::desktopFileName);

    KAboutData::setApplicationData(aboutData);

    QSurfaceFormat fmt;
    fmt.setDepthBufferSize(24); // FIXME random depth either explain why or figure out something else
    QSurfaceFormat::setDefaultFormat(fmt);

    auto engine = new QQmlApplicationEngine;

    static auto l10nContext = new KLocalizedContext(engine);
    l10nContext->setTranslationDomain(QStringLiteral(TRANSLATION_DOMAIN));
    engine->rootContext()->setContextObject(l10nContext);

    const QUrl mainUrl(u"qrc:/ui/main.qml"_s);
    QObject::connect(
        engine, &QQmlApplicationEngine::objectCreated, engine, [mainUrl](QObject *obj, const QUrl &objUrl) {
            if (!obj && mainUrl == objUrl) {
                qWarning() << "Failed to load QML dialog, falling back to QWidget.";
                abort();
            }
        });
    engine->load(mainUrl);

    return app.exec();
}
