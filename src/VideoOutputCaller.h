// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#pragma once

#include <vlc/vlc.h>

namespace VideoOutputCaller
{
bool setup_cb(void **opaque, const libvlc_video_setup_device_cfg_t *cfg, libvlc_video_setup_device_info_t *out);
void cleanup_cb(void *opaque);
void window_cb(void *opaque,
               libvlc_video_output_resize_cb report_size_change,
               libvlc_video_output_mouse_move_cb report_mouse_move,
               libvlc_video_output_mouse_press_cb report_mouse_pressed,
               libvlc_video_output_mouse_release_cb report_mouse_released,
               void *report_opaque);
bool update_output_cb(void *opaque, const libvlc_video_render_cfg_t *cfg, libvlc_video_output_cfg_t *output);
void swap_cb(void *opaque);
bool makeCurrent_cb(void *opaque, bool enter);
void *getProcAddress_cb(void *opaque, const char *fct_name);
void metadata_cb(void *opaque, libvlc_video_metadata_type_t type, const void *metadata);
bool select_plane_cb(void *opaque, size_t plane, void *output);
} // namespace VideoOutputCaller
