// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#include "Dialog.h"

#include <QDebug>

Dialog::Dialog(LibVLC *vlc)
{
    const libvlc_dialog_cbs callbacks {
        .pf_display_login = DialogCaller::pf_display_login,
        .pf_display_question = DialogCaller::pf_display_question,
        .pf_display_progress = DialogCaller::pf_display_progress,
        .pf_cancel = DialogCaller::pf_cancel,
        .pf_update_progress = DialogCaller::pf_update_progress,
    };
    libvlc_dialog_set_callbacks(vlc->get(), &callbacks, this);
    libvlc_dialog_set_error_callback(vlc->get(), DialogCaller::error, this);
}

void DialogCaller::error(void *p_data, const char *psz_title, const char *psz_text)
{
    Q_EMIT static_cast<Dialog*>(p_data)->error(QString::fromUtf8(psz_title), QString::fromUtf8(psz_text));
}

void DialogCaller::pf_display_login(void *p_data,
                                    libvlc_dialog_id *p_id,
                                    const char *psz_title,
                                    const char *psz_text,
                                    const char *psz_default_username,
                                    bool b_ask_store)
{
    // SFTP / SMB login data (usually shouldn't happen because we handle most protocols through KIO)
    qDebug() << Q_FUNC_INFO;
    Q_EMIT static_cast<Dialog *>(p_data)->displayLogin(intptr_t(p_id),
                                                       QString::fromUtf8(psz_title),
                                                       QString::fromUtf8(psz_text),
                                                       QString::fromUtf8(psz_default_username),
                                                       b_ask_store);
}

void DialogCaller::pf_display_question(void *p_data,
                                       libvlc_dialog_id *p_id,
                                       const char *psz_title,
                                       const char *psz_text,
                                       libvlc_dialog_question_type i_type,
                                       const char *psz_cancel,
                                       const char *psz_action1,
                                       const char *psz_action2)
{
    qDebug() << Q_FUNC_INFO;
    Q_EMIT static_cast<Dialog *>(p_data)->displayQuestion(intptr_t(p_id),
                                                          QString::fromUtf8(psz_title),
                                                          QString::fromUtf8(psz_text),
                                                          Dialog::QuestionType(i_type),
                                                          QString::fromUtf8(psz_cancel),
                                                          QString::fromUtf8(psz_action1),
                                                          QString::fromUtf8(psz_action2));
}

void DialogCaller::pf_display_progress(void *p_data,
                                       libvlc_dialog_id *p_id,
                                       const char *psz_title,
                                       const char *psz_text,
                                       bool b_indeterminate,
                                       float f_position,
                                       const char *psz_cancel)
{
    // On linux effectively only appears for broken avi files when rebuilding the index.
    // On windows this also appears for VLC updates (not applicable) and apparently font cache building.
    // All in all very niche.
    qWarning() << Q_FUNC_INFO;
}

void DialogCaller::pf_cancel(void *p_data, libvlc_dialog_id *p_id)
{
    qDebug() << Q_FUNC_INFO;
    Q_EMIT static_cast<Dialog *>(p_data)->cancel(intptr_t(p_id));
}

void DialogCaller::pf_update_progress(void *p_data, libvlc_dialog_id *p_id, float f_position, const char *psz_text)
{
    qWarning() << Q_FUNC_INFO;
}

void Dialog::dismiss(intptr_t id)
{
    qDebug() << Q_FUNC_INFO;
    libvlc_dialog_dismiss(reinterpret_cast<libvlc_dialog_id *>(id));
}

void Dialog::login(intptr_t id, const QString &user, const QString &password, bool store)
{
    qDebug() << Q_FUNC_INFO;
    libvlc_dialog_post_login(reinterpret_cast<libvlc_dialog_id *>(id), qUtf8Printable(user), qUtf8Printable(password), store);
}
