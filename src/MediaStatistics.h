// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#pragma once

#include <vlc/vlc.h>

#include <QObject>
#include <QTimer>
#include <QAbstractListModel>
#include <QQmlParserStatus>
#include <QMetaEnum>

class Media;

class MediaStatistics : public QAbstractListModel, public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)
    Q_PROPERTY(Media *media MEMBER m_media WRITE connectMedia NOTIFY statisticsChanged)
public:
    enum class Role { Key = Qt::UserRole, Value };
    Q_ENUM(Role);
    void connectMedia(Media *media);
    void disconnectMedia();


    void classBegin() override{}
    void componentComplete() override
    {
    }

    [[nodiscard]] int rowCount(const QModelIndex &parent) const override
    {
        return statistics().size();
    }

    [[nodiscard]] QVariant data(const QModelIndex &index, int intRole) const override
    {
        if (!index.isValid()) {
            return {};
        }

        switch (Role(intRole)) {
        case Role::Key:
            return statistics().keys().at(index.row());
        case Role::Value:
            return statistics().values().at(index.row());
        }

        return {};
    }

    [[nodiscard]] QHash<int, QByteArray> roleNames() const override
    {
        static QHash<int, QByteArray> roles;
        if (!roles.isEmpty()) {
            return roles;
        }

        roles = QAbstractListModel::roleNames();
        const QMetaEnum roleEnum = QMetaEnum::fromType<Role>();
        for (int i = 0; i < roleEnum.keyCount(); ++i) {
            const int value = roleEnum.value(i);
            Q_ASSERT(value != -1);
            roles[static_cast<int>(value)] = QByteArray("ROLE_") + roleEnum.valueToKey(value);
        }
        return roles;
    }

    QVariantMap statistics() const;
    Q_SIGNAL void statisticsChanged();

private:
    libvlc_media_stats_t m_stats;
    QTimer m_pollTimer;
    Media *m_media = nullptr;
};
