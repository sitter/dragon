// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#include "VideoOutputCaller.h"

#include "VideoOutput.h"

bool VideoOutputCaller::setup_cb(void **opaque,
                          const libvlc_video_setup_device_cfg_t *cfg,
                          libvlc_video_setup_device_info_t *out)
{
    qDebug() << Q_FUNC_INFO;
    return static_cast<VideoOutput *>(*opaque)->setup(cfg, out);
}

void VideoOutputCaller::cleanup_cb(void *opaque)
{
    qDebug() << Q_FUNC_INFO;
    static_cast<VideoOutput *>(opaque)->cleanup();
}

void VideoOutputCaller::window_cb(void *opaque,
                                  libvlc_video_output_resize_cb report_size_change,
                                  libvlc_video_output_mouse_move_cb report_mouse_move,
                                  libvlc_video_output_mouse_press_cb report_mouse_pressed,
                                  libvlc_video_output_mouse_release_cb report_mouse_released,
                                  void *report_opaque)
{
    qDebug() << Q_FUNC_INFO;
    static_cast<VideoOutput *>(opaque)->window(report_size_change, report_mouse_move, report_mouse_pressed, report_mouse_released, report_opaque);
}

bool VideoOutputCaller::update_output_cb(void *opaque, const libvlc_video_render_cfg_t *cfg, libvlc_video_output_cfg_t *output)
{
    qDebug() << Q_FUNC_INFO;
    return static_cast<VideoOutput *>(opaque)->update_output(cfg, output);
}

void VideoOutputCaller::swap_cb(void *opaque)
{
    // qDebug() << Q_FUNC_INFO;
    static_cast<VideoOutput *>(opaque)->swap();
}

bool VideoOutputCaller::makeCurrent_cb(void *opaque, bool enter)
{
    // qDebug() << Q_FUNC_INFO << enter;
    return static_cast<VideoOutput *>(opaque)->makeCurrent(enter);
}

void *VideoOutputCaller::getProcAddress_cb(void *opaque, const char *fct_name)
{
    return static_cast<VideoOutput *>(opaque)->getProcAddress(fct_name);
}

void VideoOutputCaller::metadata_cb(void *opaque, libvlc_video_metadata_type_t type, const void *metadata)
{
    qDebug() << Q_FUNC_INFO;
    static_cast<VideoOutput *>(opaque)->metadata(type, metadata);
}

bool VideoOutputCaller::select_plane_cb(void *opaque, size_t plane, void *output)
{
    qDebug() << Q_FUNC_INFO;
    return static_cast<VideoOutput *>(opaque)->select_plane(plane, output);
}
