// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#pragma once

#include <vlc/vlc.h>

#include <memory>

#include <QQuickItem>

class VideoRenderer;
class VideoOutput;

class VideoItem : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(VideoOutput *output MEMBER m_vout CONSTANT)
public:
    VideoItem();
    ~VideoItem() override;
    Q_DISABLE_COPY_MOVE(VideoItem)

    void ensureRenderer();
    QSGNode *updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *) override;

public Q_SLOTS:
    void sync();
    void cleanup();

private Q_SLOTS:
    void handleWindowChanged(QQuickWindow *win);

private:
    void releaseResources() override;

    VideoOutput *m_vout = nullptr; // parented raw pointer so it can be a property
    std::unique_ptr<VideoRenderer> m_renderer;
};
