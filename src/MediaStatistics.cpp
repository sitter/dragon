// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#include "MediaStatistics.h"

#include <chrono>

#include "Media.h"

using namespace std::chrono_literals;

void MediaStatistics::connectMedia(Media *media)
{
    disconnect();
    disconnectMedia();
    if (!media) {
        return;
    }

    m_media = media;

    m_pollTimer.setInterval(500ms);
    connect(&m_pollTimer, &QTimer::timeout, this, [this] {
        if (m_media) {
            libvlc_media_get_stats(m_media->get(), &m_stats);
            Q_EMIT statisticsChanged();
        }
    });
    m_pollTimer.start();
}

void MediaStatistics::disconnectMedia()
{
    if (!m_media) {
        return;
    }

    m_pollTimer.stop();
    m_pollTimer.disconnect(this);
    m_media = nullptr;
}

QVariantMap MediaStatistics::statistics() const
{
    return {
        {QStringLiteral("i_read_bytes"), m_stats.i_read_bytes},
        {QStringLiteral("f_input_bitrate"), m_stats.f_input_bitrate},
        {QStringLiteral("i_demux_read_bytes"), m_stats.i_demux_read_bytes},
        {QStringLiteral("f_demux_bitrate"), m_stats.f_demux_bitrate},
        {QStringLiteral("i_demux_corrupted"), m_stats.i_demux_corrupted},
        {QStringLiteral("i_demux_discontinuity"), m_stats.i_demux_discontinuity},
        {QStringLiteral("i_decoded_video"), m_stats.i_decoded_video},
        {QStringLiteral("i_decoded_audio"), m_stats.i_decoded_audio},
        {QStringLiteral("i_displayed_pictures"), m_stats.i_displayed_pictures},
        {QStringLiteral("i_late_pictures"), m_stats.i_late_pictures},
        {QStringLiteral("i_lost_pictures"), m_stats.i_lost_pictures},
        {QStringLiteral("i_played_abuffers"), m_stats.i_played_abuffers},
        {QStringLiteral("i_lost_abuffers"), m_stats.i_lost_abuffers},
    };
}
