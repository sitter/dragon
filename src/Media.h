// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#pragma once

#include <vlc/vlc.h>

#include <memory>

#include <QObject>

namespace std
{
template<>
struct default_delete<libvlc_media_t> {
    void operator()(libvlc_media_t *ptr) const
    {
        libvlc_media_release(ptr);
    }
};
} // namespace std

class Media : public QObject
{
    Q_OBJECT
public:
    enum class Retain { No = false, Yes = true };
    explicit Media(const QUrl &mrl, QObject *parent = nullptr);
    explicit Media(libvlc_media_t *media, Retain retain = Retain::Yes, QObject *parent = nullptr);

    QUrl mrl() const;

    libvlc_media_t *get() const { return m_media.get(); }

private:
    const std::unique_ptr<libvlc_media_t> m_media;
};

namespace IOCaller
{
int open(void *opaque, void **datap, uint64_t *sizep);
ptrdiff_t read(void *opaque, unsigned char *buf, size_t len);
int seek(void *opaque, uint64_t offset);
void close(void *opaque);
} // namespace MediaCaller
