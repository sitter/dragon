// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#include "VideoOutput.h"

#include <EGL/egl.h>

void *VideoOutput::getProcAddress(const char *fct_name)
{
    // https://code.videolan.org/videolan/vlc/-/issues/26813
    if (QByteArray(fct_name).startsWith(QByteArrayLiteral("egl"))) {
        return reinterpret_cast<void *>(eglGetProcAddress(fct_name));
    }
    const auto func = m_context.getProcAddress(fct_name);
    Q_ASSERT(func);
    return reinterpret_cast<void *>(func);
}
VideoOutput::VideoOutput(QQuickWindow *window)
{
    // connection is direct so the context is definitely still alive. this is thread safe because of the semaphore
    connect(
        window,
        &QQuickWindow::sceneGraphInitialized,
        this,
        [this, window]() {
            auto context = QOpenGLContext::currentContext();
            QMutexLocker lock2(&m_windowCallbacksMutex);
            qDebug() << "Making context";
            m_surface.setFormat(window->format());
            m_surface.create();

            m_context.setFormat(window->format());
            m_context.setShareContext(context);
            m_context.create();
            connect(&m_context, &QOpenGLContext::aboutToBeDestroyed, this, [] {
                qWarning() << "about aboutToBeDestroyed";
            });

            m_initializedSemaphore.release();
        },
        Qt::DirectConnection);
    connect(window, &QQuickWindow::sceneGraphAboutToStop, this, [this] { m_dead = true; });
}

void VideoOutput::setViewportSize(const QSize &size)
{
    if (size != m_viewportSize) {
        m_viewportSize = size;
        doResize();
    }
}

bool VideoOutput::setup(const libvlc_video_setup_device_cfg_t *cfg, libvlc_video_setup_device_info_t *out)
{
    Q_UNUSED(cfg);
    Q_UNUSED(out);

    if (!QOpenGLContext::supportsThreadedOpenGL()) {
        abort();
        return false;
    }

    m_initializedSemaphore.acquire();

    m_width = 0;
    m_height = 0;
    return true;
}

void VideoOutput::cleanup()
{
    m_initializedSemaphore.release();
    m_garbage = nullptr;
    m_fbo = nullptr;
}

void VideoOutput::doResize()
{
    // Schedule a resize update. We queue this to avoid deadlocks between threads. Resizes may be initiated by
    // the RenderThread while the setup_cb is waiting for the RenderThread to return.
    QMetaObject::invokeMethod(
        this,
        [this] {
            QMutexLocker lock(&m_windowCallbacksMutex);
            if (!m_windowCallbacks.has_value() || m_viewportSize.isEmpty() || m_viewportSize == QSize(int(m_width), int(m_height))) {
                return;
            }
            qDebug() << "doResize" << m_viewportSize;
            const auto size = m_viewportSize;
            m_windowCallbacks->resize(m_windowCallbacks->opaque, size.width(), size.height());
        },
        Qt::QueuedConnection);
}

void VideoOutput::mouseMove(int x, int y)
{
    // We queue this to avoid deadlocks between threads.
    QMetaObject::invokeMethod(this, [this, x, y] {
        QMutexLocker lock(&m_windowCallbacksMutex);
        if (m_windowCallbacks.has_value()) {
            m_windowCallbacks->mouseMove(m_windowCallbacks->opaque, x, y);
            }
        },
        Qt::QueuedConnection);
}

void VideoOutput::mousePress(libvlc_video_output_mouse_button_t button)
{
    // We queue this to avoid deadlocks between threads.
    QMetaObject::invokeMethod(this, [this, button] {
        QMutexLocker lock(&m_windowCallbacksMutex);
        if (m_windowCallbacks.has_value()) {
            m_windowCallbacks->mousePress(m_windowCallbacks->opaque, button);
            }
        },
        Qt::QueuedConnection);
}

void VideoOutput::mouseRelease(libvlc_video_output_mouse_button_t button)
{
    // We queue this to avoid deadlocks between threads.
    QMetaObject::invokeMethod(this, [this, button] {
        QMutexLocker lock(&m_windowCallbacksMutex);
        if (m_windowCallbacks.has_value()) {
            m_windowCallbacks->mouseRelease(m_windowCallbacks->opaque, button);
            }
        },
        Qt::QueuedConnection);
}

void VideoOutput::window(libvlc_video_output_resize_cb report_size_change,
                         libvlc_video_output_mouse_move_cb report_mouse_move,
                         libvlc_video_output_mouse_press_cb report_mouse_pressed,
                         libvlc_video_output_mouse_release_cb report_mouse_released,
                         void *report_opaque)
{
#warning TODO mouse interaction
    qDebug() << Q_FUNC_INFO;
    {
        QMutexLocker lock(&m_windowCallbacksMutex);
        qDebug() << "~~~" << report_size_change;
        m_windowCallbacks = VLCWindowCallbacks {.resize = report_size_change,
                                                .mouseMove = report_mouse_move,
                                                .mousePress = report_mouse_pressed,
                                                .mouseRelease = report_mouse_released,
                                                .opaque = report_opaque};
    }
    doResize();
}

bool VideoOutput::update_output(const libvlc_video_render_cfg_t *cfg, libvlc_video_output_cfg_t *output)
{
    QMutexLocker lock(&m_fboMutex);
    if (cfg->width != m_width || cfg->height != m_height) {
        qDebug() << "moving from" << QSize(m_width, m_height) << "to" << QSize(cfg->width, cfg->height);
        m_garbage.swap(m_fbo);
        m_fbo = std::make_unique<QOpenGLFramebufferObject>(cfg->width, cfg->height);
        m_fbo->bind();
        qDebug() << "m_fbo" << m_fbo.get() << "m_garbage" << m_garbage.get();

        m_width = cfg->width;
        m_height = cfg->height;
    }

    output->opengl_format = GL_RGBA;
    output->full_range = true;
    output->colorspace = libvlc_video_colorspace_BT709;
    output->primaries = libvlc_video_primaries_BT709;
    output->transfer = libvlc_video_transfer_func_SRGB;
    output->orientation = libvlc_video_orient_top_left;

    return true;
}

void VideoOutput::swap()
{
    QMutexLocker locker(&m_fboMutex);
    m_updated = true;
    Q_EMIT frameReady();
}

bool VideoOutput::makeCurrent(bool enter)
{
    if (m_dead) {
        return false; // stop touching the context immediately! otherwise we run risk of crashing on q_global_statics
                      // inside the context. no better way to prevent this.
    }
    if (enter) {
        return m_context.makeCurrent(&m_surface);
    }
    if (!QOpenGLContext::currentContext()) {
        return false;
    }
    m_context.doneCurrent();
    return true;
}

void VideoOutput::metadata(libvlc_video_metadata_type_t type, const void *metadata)
{
#warning todo
    Q_ASSERT(false);
}

bool VideoOutput::select_plane(size_t plane, void *output)
{
#warning todo
    Q_ASSERT(false);
    return false;
}

QOpenGLFramebufferObject *VideoOutput::getVideoFrame()
{
    QMutexLocker locker(&m_fboMutex);
    if (m_updated) {
        m_updated = false;
    }
    return m_fbo.get();
}
