// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#pragma once

#include <vlc/vlc.h>

#include <QAbstractListModel>
#include <QDebug>
#include <QMetaEnum>
#include <QQmlParserStatus>

#include "MediaPlayer.h"

namespace std
{
template<>
struct default_delete<libvlc_media_track_t> {
    void operator()(libvlc_media_track_t *ptr) const
    {
        libvlc_media_track_release(ptr);
    }
};
} // namespace std

class MediaTrack : public QObject
{
    Q_OBJECT

public:
    explicit MediaTrack(libvlc_media_track_t *track, QObject *parent = nullptr)
        : QObject(parent)
        , m_track(track)
    {
        // Hold the track so we don't have to copy everything out of it and instead can insist on it staying alive for
        // however long the frontend object is alive.
        libvlc_media_track_hold(m_track.get()); // released by deleter
    }

#warning todo
    // private:
    std::unique_ptr<libvlc_media_track_t> m_track;
};

namespace std
{
template<>
struct default_delete<libvlc_media_tracklist_t> {
    void operator()(libvlc_media_tracklist_t *ptr) const
    {
        libvlc_media_tracklist_delete(ptr);
    }
};
} // namespace std

class MediaTrackModelBase : public QAbstractListModel, public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)
    Q_PROPERTY(MediaPlayer *player MEMBER m_player NOTIFY playerChanged REQUIRED)
public:
    enum class Role { Selected = Qt::UserRole };
    Q_ENUM(Role)

    Q_SIGNAL void playerChanged();

protected:
    using QAbstractListModel::QAbstractListModel;
    MediaPlayer *m_player = nullptr;
};

template <libvlc_track_type_t VLCTrackType>
class MediaTrackModel : public MediaTrackModelBase
{
public:
    void classBegin() override{}
    void componentComplete() override
    {
        #warning todo this needs to run on all media changes
        Q_ASSERT(m_player);

        auto reload = [this] {
            m_trackList = std::unique_ptr<libvlc_media_tracklist_t>(libvlc_media_player_get_tracklist(m_player->get(), VLCTrackType, false));
            if (!m_trackList) {
#warning todo
                return;
            }
            const auto count = libvlc_media_tracklist_count(m_trackList.get());
            beginResetModel();
            m_tracks.clear();
            for (size_t i = 0; i < count; ++i) {
                m_tracks.push_back(std::make_unique<MediaTrack>(libvlc_media_tracklist_at(m_trackList.get(), i)));
            }
            endResetModel();
        };

        connect(m_player, &MediaPlayer::tracksChanged, this, reload);
        reload();
    }

    [[nodiscard]] int rowCount(const QModelIndex &parent) const override
    {
        return m_tracks.size();
    }

    [[nodiscard]] QVariant data(const QModelIndex &index, int intRole) const override
    {
        if (!index.isValid()) {
            return {};
        }

        auto &track = m_tracks.at(index.row());
        switch (intRole) {
        case Qt::DisplayRole:
            return QString::fromUtf8(track->m_track->psz_name);
        }

        switch (static_cast<Role>(intRole)) {
        case Role::Selected:
            return track->m_track->selected;
        }

        return {};
    }

    bool setData(const QModelIndex &index, const QVariant &value, int intRole = Qt::EditRole) override
    {
        if (!index.isValid()) {
            return false;
        }

        auto &track = m_tracks.at(index.row());
        switch (static_cast<Role>(intRole)) {
        case Role::Selected:
            libvlc_media_player_select_track(m_player->get(), track->m_track.get());
            return true;
        }

        return false;
    }

    [[nodiscard]] QHash<int, QByteArray> roleNames() const override
    {
        static QHash<int, QByteArray> roles ;
        if (!roles.isEmpty()) {
            return roles;
        }

        roles = QAbstractListModel::roleNames();
        const QMetaEnum roleEnum = QMetaEnum::fromType<Role>();
        for (int i = 0; i < roleEnum.keyCount(); ++i) {
            const int value = roleEnum.value(i);
            Q_ASSERT(value != -1);
            roles[static_cast<int>(value)] = QByteArray("ROLE_") + roleEnum.valueToKey(value);
        }
        return roles;
    }

private:
    std::unique_ptr<libvlc_media_tracklist_t> m_trackList;
    std::vector<std::unique_ptr<MediaTrack>> m_tracks;
};

