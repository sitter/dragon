// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#include "MediaPlayer.h"

#include <chrono>

#include <QCoreApplication>
#include <QScopeGuard>
#include <QTimer>

#include "VideoOutputCaller.h"

using namespace std::chrono_literals;
using namespace Qt::StringLiterals;

MediaPlayer::MediaPlayer(LibVLC *vlc, QObject *parent)
    : QObject(parent)
    , m_player(libvlc_media_player_new(vlc->get()))
{
    auto manager = libvlc_media_player_event_manager(m_player.get());
    static constexpr auto events = {
        libvlc_MediaPlayerMediaChanged,
        libvlc_MediaPlayerNothingSpecial,
        libvlc_MediaPlayerOpening,
        libvlc_MediaPlayerBuffering,
        libvlc_MediaPlayerPlaying,
        libvlc_MediaPlayerPaused,
        libvlc_MediaPlayerStopped,
        libvlc_MediaPlayerForward,
        libvlc_MediaPlayerBackward,
        libvlc_MediaPlayerStopping,
        libvlc_MediaPlayerEncounteredError,
        libvlc_MediaPlayerTimeChanged,
        libvlc_MediaPlayerPositionChanged,
        libvlc_MediaPlayerSeekableChanged,
        libvlc_MediaPlayerPausableChanged,
        libvlc_MediaPlayerSnapshotTaken,
        libvlc_MediaPlayerLengthChanged,
        libvlc_MediaPlayerVout,
        libvlc_MediaPlayerCorked,
        libvlc_MediaPlayerUncorked,
        libvlc_MediaPlayerMuted,
        libvlc_MediaPlayerUnmuted,
        libvlc_MediaPlayerAudioVolume,
        libvlc_MediaPlayerESAdded,
        libvlc_MediaPlayerESDeleted,
        libvlc_MediaPlayerESUpdated,
        libvlc_MediaPlayerESSelected,
    };
    for (const auto &event : events) {
        libvlc_event_attach(manager, event, event_cb, this);
    }
    libvlc_media_player_set_video_title_display(m_player.get(), libvlc_position_disable, 0);
    connect(qApp, &QCoreApplication::aboutToQuit, this, [this] {
        // Try to make sure the player is stopped properly, VLC doesn't like getting hit over the head.
        if (m_stopped) {
            return;
        }

        qDebug() << "stopping";
        QEventLoop loop;
        connect(this, &MediaPlayer::stoppedChanged, &loop, &QEventLoop::quit);
        QTimer::singleShot(1s, &loop, &QEventLoop::quit);
        setStop();
        loop.exec();
        qDebug() << "stop done";
    });
}

void MediaPlayer::event_cb(const libvlc_event_t *event, void *opaque)
{
    auto that = static_cast<MediaPlayer *>(opaque);
    Q_ASSERT(that);

    // const auto moveToRightThread = []() {}

    // Do not forget to register for the events you want to handle here!
    switch (event->type) {
    case libvlc_MediaPlayerTimeChanged: {
        const libvlc_time_t time = event->u.media_player_time_changed.new_time;
        QMetaObject::invokeMethod(that, [that, time]() {
            that->m_time = time;
            Q_EMIT that->timeChanged();
        });
        break;
    }
    case libvlc_MediaPlayerSeekableChanged: {
        const auto seekable = event->u.media_player_seekable_changed.new_seekable;
        QMetaObject::invokeMethod(that, [that, seekable]() {
            that->m_seekable = seekable;
            Q_EMIT that->seekableChanged();
        });
        break;
    }
    case libvlc_MediaPlayerLengthChanged: {
        const auto length = event->u.media_player_length_changed.new_length;
        QMetaObject::invokeMethod(that, [that, length]() {
            that->m_length = length;
            Q_EMIT that->lengthChanged();
        });
        break;
    }
    case libvlc_MediaPlayerESDeleted:
    case libvlc_MediaPlayerESUpdated:
    case libvlc_MediaPlayerESSelected:
    case libvlc_MediaPlayerESAdded: {
        const auto length = event->u.media_player_length_changed.new_length;
        QMetaObject::invokeMethod(that, [that, length]() {
            Q_EMIT that->tracksChanged();
        });
        break;
    }
    // case libvlc_MediaPlayerNothingSpecial:
    //     P_EMIT_STATE(NoState);
    //     break;
    // case libvlc_MediaPlayerOpening:
    //     P_EMIT_STATE(OpeningState);
    //     break;
    // case libvlc_MediaPlayerBuffering:
    //     QMetaObject::invokeMethod(
    //                 that, "bufferChanged",
    //                 Qt::QueuedConnection,
    //                 Q_ARG(int, event->u.media_player_buffering.new_cache));
    //     break;
    case libvlc_MediaPlayerPlaying: {
        QMetaObject::invokeMethod(that, [that]() {
            that->m_paused = false;
            Q_EMIT that->pausedChanged();
            that->m_stopped = false;
            Q_EMIT that->stoppedChanged();
        });
        break;
    }
    case libvlc_MediaPlayerPaused: {
        QMetaObject::invokeMethod(that, [that]() {
            that->m_paused = true;
            Q_EMIT that->pausedChanged();
            that->m_stopped = false;
            Q_EMIT that->stoppedChanged();
        });
        break;
    }
    case libvlc_MediaPlayerStopped: {
        QMetaObject::invokeMethod(that, [that]() {
            that->m_paused = false;
            Q_EMIT that->pausedChanged();
            that->m_stopped = true;
            Q_EMIT that->stoppedChanged();
        });
        break;
    }
    // case libvlc_MediaPlayerEndReached:
    //     P_EMIT_STATE(EndedState);
    //     break;
    case libvlc_MediaPlayerEncounteredError:
        qWarning() << "libvlc_MediaPlayerEncounteredError" << libvlc_errmsg();
        break;
    // case libvlc_MediaPlayerVout:
    //     if (event->u.media_player_vout.new_count > 0) {
    //         P_EMIT_HAS_VIDEO(true);
    //     } else {
    //         P_EMIT_HAS_VIDEO(false);
    //     }
    //     break;
    case libvlc_MediaPlayerMediaChanged: {
        QMetaObject::invokeMethod(that, [that]() {
            Q_EMIT that->mediaChanged();
        });
        break;
    }
    // case libvlc_MediaPlayerCorked:
    //     that->pause();
    //     break;
    // case libvlc_MediaPlayerUncorked:
    //     that->play();
    //     break;
    case libvlc_MediaPlayerMuted: {
        QMetaObject::invokeMethod(that, [that]() {
            that->m_muted = true;
            Q_EMIT that->mutedChanged();
        });
        break;
    }
    case libvlc_MediaPlayerUnmuted: {
        QMetaObject::invokeMethod(that, [that]() {
            that->m_muted = false;
            Q_EMIT that->mutedChanged();
        });
        break;
    }
    case libvlc_MediaPlayerAudioVolume: {
        const auto volume = event->u.media_player_audio_volume.volume;
        QMetaObject::invokeMethod(that, [that, volume]() {
            that->m_volume = volume * 100;
            Q_EMIT that->volumeChanged();
        });
        break;
    }
    // case libvlc_MediaPlayerForward:
    // case libvlc_MediaPlayerBackward:
    // case libvlc_MediaPlayerPositionChanged:
    case libvlc_MediaPlayerPausableChanged:
        const bool pausable = event->u.media_player_pausable_changed.new_pausable;
        QMetaObject::invokeMethod(that, [that, pausable]() {
            that->m_pausable = pausable;
            Q_EMIT that->pausableChanged();
        });
        break;
    // case libvlc_MediaPlayerSnapshotTaken: // Snapshot call is sync, so this is useless.
    //     break;
    //     QString msg = QString("Unknown event: ") + QString(libvlc_event_type_name(event->type));
    //     Q_ASSERT_X(false, "event_cb", qPrintable(msg));
    //     break;
    }
}

void MediaPlayer::seek(qint64 time, bool fast)
{
    libvlc_media_player_set_time(m_player.get(), time, false);
    // set the target time, this will be updated when the time change event arrives for the actual time. In the meantime
    // this makes sure we report something possibly accurate for the seekslider.
    m_time = time;
    Q_EMIT timeChanged();
}

void MediaPlayer::setVideoOutput(VideoOutput *videoOutput)
{
    if (!libvlc_video_set_output_callbacks(m_player.get(),
                                           libvlc_video_engine_opengl,
                                           VideoOutputCaller::setup_cb,
                                           VideoOutputCaller::cleanup_cb,
                                           VideoOutputCaller::window_cb,
                                           VideoOutputCaller::update_output_cb,
                                           VideoOutputCaller::swap_cb,
                                           VideoOutputCaller::makeCurrent_cb,
                                           VideoOutputCaller::getProcAddress_cb,
                                           VideoOutputCaller::metadata_cb,
                                           VideoOutputCaller::select_plane_cb,
                                           videoOutput)) {
                    Q_UNREACHABLE();
#warning todo error handling
    }
    Q_EMIT videoOutputChanged();
}

bool MediaPlayer::play()
{
    if (libvlc_media_player_play(m_player.get()) != 0) {
        qWarning() << libvlc_errmsg();
        return false;
    }
    return true;
}

bool MediaPlayer::play(const QString &path)
{
#warning fixme this royally falls on the nose when the path contains hash symbols or question marks
    const QUrl maybeUrl = QUrl::fromUserInput(path);
    if (!maybeUrl.scheme().isEmpty()) {
        return play(maybeUrl);
    }

    QUrl url;
    url.setScheme("file"_L1);
    url.setPath(path);
    return play(url);
}

bool MediaPlayer::play(const QUrl &url)
{
    return play(std::make_unique<Media>(url));
}

bool MediaPlayer::play(std::unique_ptr<Media> media)
{
    m_media = std::move(media);
    libvlc_media_player_set_media(m_player.get(), m_media->get());
    Q_EMIT mediaChanged();
    return play();
}

void MediaPlayer::togglePause()
{
    libvlc_media_player_set_pause(m_player.get(), !m_paused);
}

void MediaPlayer::setVolume(int volume)
{
    // FIXME volume shouldn't be linear, should it?
    libvlc_audio_set_volume(m_player.get(), volume);
}

void MediaPlayer::setMuted(bool mute)
{
    libvlc_audio_set_mute(m_player.get(), mute);
}

void MediaPlayer::setStop(bool stop)
{
    if (stop) {
        qDebug() << Q_FUNC_INFO;
        libvlc_media_player_stop_async(m_player.get());
    }
}

Media *MediaPlayer::media()
{
    return m_media.get();
}
