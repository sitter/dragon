// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#pragma once

#include <vlc/vlc.h>

#include <memory>

#include <QObject>

namespace std
{
template<>
struct default_delete<libvlc_instance_t> {
    void operator()(libvlc_instance_t *ptr) const
    {
        libvlc_release(ptr);
    }
};
} // namespace std

class LibVLC : public QObject
{
public:
    static LibVLC *instance();
    libvlc_instance_t *get() const { return m_vlc.get(); }

    static std::tuple<int, const char *const *> defaultArguments();
    std::unique_ptr<libvlc_instance_t> m_vlc {std::apply(libvlc_new, defaultArguments())};

private:
    explicit LibVLC(QObject *parent = nullptr);
};
