// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#include "LibVLC.h"

#include <QGuiApplication>
#include <QIcon>

#include "dragon.h"

using namespace Qt::StringLiterals;

std::tuple<int, const char *const *> LibVLC::defaultArguments()
{
    static constexpr std::array args {
        "--file-logging",
        // Cheekily stolen from Phonon
        "--no-media-library",
        "--no-osd",
        "--no-stats",
        // By default VLC will put a picture-in-picture when making a snapshot.
        // This is unexpected behaviour for us, so we force it off.
        "--no-snapshot-preview",
        // Do not load xlib dependent modules as we cannot ensure proper init
        // order as expected by xlib thus leading to crashes.
        // KDE BUG: 240001
        "--no-xlib",
        // Do not preload services discovery modules, we don't use them.
        "--services-discovery=''",
        // The application is meant to manage this. Also, using the builtin
        // inhibitor may cause problems on shutdown if VLC tries to uninhibit too
        // late in the application lifecycle.
        "--disable-screensaver=0"
    };
    return {args.size(), args.data()};
}

LibVLC *LibVLC::instance()
{
    static LibVLC vlc;
    return &vlc;
}

LibVLC::LibVLC(QObject *parent)
    : QObject(parent)
{
    libvlc_set_user_agent(get(),
                          qUtf8Printable(u"Dragon Player %1"_s.arg(Dragon::version)),
                          qUtf8Printable(u"Dragon/%1"_s.arg(Dragon::version)));
    libvlc_set_app_id(get(),
                      qUtf8Printable(qGuiApp->applicationDisplayName()),
                      qUtf8Printable(Dragon::version),
                      qUtf8Printable(qGuiApp->windowIcon().name()));
}
