// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#pragma once

#include <QDBusAbstractAdaptor>

// TODO Temporary
#include <QMetaObject>
#include <QMetaProperty>
#include <QMetaMethod>
#include <QDebug>

class Adaptor: public QDBusAbstractAdaptor
{
    Q_OBJECT
public:
    explicit Adaptor(QObject *parent) : QDBusAbstractAdaptor(parent) {}

    QObject *m_other = nullptr;

    static void assertPropertyConsistency(const QMetaObject *mo, const QMetaObject *otherMo)
    {
        for (auto i = mo->propertyOffset(); i < mo->propertyCount(); ++i) {
            const auto property = mo->property(i);
            const QString dbusName = QString::fromLatin1(property.name());
            const QString name = dbusName.at(0).toLower() + dbusName.mid(1);

            const auto otherPropertyIndex = otherMo->indexOfProperty(qUtf8Printable(name));
            Q_ASSERT_X(otherPropertyIndex >= 0,
                       Q_FUNC_INFO,
                       qUtf8Printable(QStringLiteral("property %1 missing in parent (qml-side)").arg(name)));
            const auto otherProperty = otherMo->property(otherPropertyIndex);
            Q_ASSERT_X(
                // NB: when the property isn't notifable on the dbus side it doesn't matter what the state on the qml
                // side is
                property.hasNotifySignal() ? property.hasNotifySignal() == otherProperty.hasNotifySignal() : true,
                Q_FUNC_INFO,
                qUtf8Printable(
                    QStringLiteral("property %1 doesn't have notify state on qml side (dbus-side=%2; qml-side=%3)")
                        .arg(name, QString::number(property.hasNotifySignal()), QString::number(otherProperty.hasNotifySignal()))));

            if (!property.isWritable()) {
                continue;
            }

            const auto otherSetterIndex = otherMo->indexOfMethod(qUtf8Printable(QStringLiteral("set") + dbusName + QStringLiteral("(QVariant)")));
            Q_ASSERT_X(otherSetterIndex >= 0,
                       Q_FUNC_INFO,
                       qUtf8Printable(QStringLiteral("set%1(QVariant) missing in parent (qml-side)").arg(dbusName)));
        }
    }

    static void assertMethodConsistency(const QMetaObject *mo, const QMetaObject *otherMo)
    {
        for (auto i = mo->methodOffset(); i < mo->methodCount(); ++i) {
            const auto method = mo->method(i);
            switch (method.methodType()) {
            case QMetaMethod::Constructor:
                continue;
            case QMetaMethod::Signal:
            case QMetaMethod::Method:
            case QMetaMethod::Slot:
                break;
            }

            if (method.name().startsWith(QByteArrayLiteral("pp"))) {
                continue; // internal to the adapator
            }

            if (method.methodType() == QMetaMethod::Signal) {
                if (method.name().endsWith("Changed")) {
                    continue;
                }
            }

            const QString dbusSignature = QString::fromLatin1(method.methodSignature());
            const QString dbusName = QString::fromLatin1(method.name());
            const QString name = dbusName.at(0).toLower() + dbusName.mid(1);
            // On the qml side all arguments are variants, to deal with this we manually build a signature for that side.
            QStringList parameters;
            for (auto i = 0; i < method.parameterCount(); ++i) {
                parameters.append(QStringLiteral("QVariant"));
            }
            const QString signature = name + QLatin1Char('(') + parameters.join(QStringLiteral(", ")) + QLatin1Char(')');

            qDebug() << "----";
            auto otherMethodIndex = otherMo->indexOfMethod(qUtf8Printable(signature));
            if (otherMethodIndex < 0 && method.methodType() == QMetaMethod::Signal) {
                // For signals the arguments don't necessarily match up. e.g. Seeked(int64) on the cpp side is seeked(int) on the qml side
                for (auto i = otherMo->methodOffset(); i < otherMo->methodCount(); ++i) {
                    const auto otherMethod = otherMo->method(i);
                    qDebug() << (otherMethod.name() == method.name()) << otherMethod.name()<< method.name();
                    if (QString::fromLatin1(otherMethod.name()) == name) {
                        otherMethodIndex = i;
                        break;
                    }
                }
            }

            Q_ASSERT_X(otherMethodIndex >= 0,
                       Q_FUNC_INFO,
                       qUtf8Printable(QStringLiteral("method %1 missing in parent (qml-side)").arg(signature)));
            // TODO check that methodType match!
        }
    }

    static void assertConsistency(const QMetaObject *mo, const QMetaObject *otherMo)
    {
        // Assert model consistency. The adaptor (dbus) and the parent (qml) should be consistent. That means:
        // - all properties in our property set exist in the parent
        // - those properties have consistent notifyable states
        // - if the property is writable on our end a suitable setFoo function must exist on the qml side
        assertPropertyConsistency(mo, otherMo);
        // - all methods in our method set exist in the parent
        assertMethodConsistency(mo, otherMo);
    }

    void bridge(QObject *other)
    {
        m_other = other;
        auto otherMo = other->metaObject();
        auto mo = metaObject();

        assertConsistency(mo, otherMo);

        // Once consistency is asserted we create the bridging for the notifications.
        for (auto i = mo->propertyOffset(); i < mo->propertyCount(); ++i) {
            const auto property = mo->property(i);
            if (!property.hasNotifySignal()) {
                continue;
            }
            const QString dbusName = QString::fromLatin1(property.name());
            const QString name = dbusName.at(0).toLower() + dbusName.mid(1);

            const auto otherPropertyIndex = otherMo->indexOfProperty(qUtf8Printable(name));
            const auto otherProperty = otherMo->property(otherPropertyIndex);

            const auto connected = connect(other, otherProperty.notifySignal(), this, property.notifySignal());
            Q_ASSERT(connected);
        }

        // ... and bridging the signals
        for (auto i = mo->methodOffset(); i < mo->methodCount(); ++i) {
            const auto method = mo->method(i);
            if (method.methodType() != QMetaMethod::Signal || method.name().endsWith("Changed")) {
                continue;
            }

            const QString dbusSignature = QString::fromLatin1(method.methodSignature());
            const QString signature = dbusSignature.at(0).toLower() + dbusSignature.mid(1);
            const QString dbusName = QString::fromLatin1(method.name());
            const QString name = dbusName.at(0).toLower() + dbusName.mid(1);

            auto otherMethodIndex = otherMo->indexOfMethod(qUtf8Printable(signature));
            if (otherMethodIndex < 0 && method.methodType() == QMetaMethod::Signal) {
                // For signals the arguments don't necessarily match up. e.g. Seeked(int64) on the cpp side is seeked(int) on the qml side
                for (auto i = otherMo->methodOffset(); i < otherMo->methodCount(); ++i) {
                    const auto otherMethod = otherMo->method(i);
                    qDebug() << (otherMethod.name() == method.name()) << otherMethod.name()<< method.name();
                    if (QString::fromLatin1(otherMethod.name()) == name) {
                        otherMethodIndex = i;
                        break;
                    }
                }
            }
            const auto otherMethod = otherMo->method(otherMethodIndex);

            qDebug() << "connecting" << otherMethod.name() << otherMethod.parameterNames() << "to" << method.methodSignature();
            const auto connected = connect(other, otherMethod, this, method);
            // Q_ASSERT(connected);
        }

        afterBridging(other);
    }

    virtual void afterBridging(QObject *other) {}

    // Hellishly awkward. QML property names may not start with capital letters, meaning we need to bridge the
    // properties out of the QML object into this here adaptor object, manually. Barf.
#define BRIDGE_PROPERTY_READABLE_WITHOUT_NOTIFY(type, name, lowerCaseName)                                                            \
    Q_PROPERTY(type name READ lowerCaseName)                                                      \
    type lowerCaseName() const                                                                                         \
    {                                                                                                                  \
        return m_other->property(#lowerCaseName).value<type>();                                                        \
    }

#define BRIDGE_PROPERTY_READABLE(type, name, lowerCaseName)                                                            \
    Q_PROPERTY(type name READ lowerCaseName NOTIFY name##Changed)                                                      \
    type lowerCaseName() const                                                                                         \
    {                                                                                                                  \
        return m_other->property(#lowerCaseName).value<type>();                                                        \
    }                                                                                                                  \
    Q_SIGNAL void name##Changed();

#define BRIDGE_PROPERTY_WRITABLE(type, name, lowerCaseName)                                                            \
    Q_PROPERTY(type name READ lowerCaseName WRITE set##name NOTIFY name##Changed)                                      \
    type lowerCaseName() const                                                                                         \
    {                                                                                                                  \
        return m_other->property(#lowerCaseName).value<type>();                                                        \
    }                                                                                                                  \
    void set##name(const type &value)                                                                                  \
    {                                                                                                                  \
        metaObject()->invokeMethod(m_other, "set" #name, Q_ARG(QVariant, value));                                      \
    }                                                                                                                  \
    Q_SIGNAL void name##Changed();

#define BRIDGE_CALL(rettype, name, lowerCaseName)                                                                      \
    rettype name()                                                                                                     \
    {                                                                                                                  \
        metaObject()->invokeMethod(m_other, Q_SLOT(#lowerCaseName));                                                   \
    }
};
