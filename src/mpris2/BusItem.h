// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#pragma once

#include <QQuickItem>

// TODO temporary
#include "Adaptor.h"
#include "KDBusPropertiesChangedAdaptor.h"

class BusItem : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QString path MEMBER m_path NOTIFY pathChanged REQUIRED)
public:
    using QQuickItem::QQuickItem;
    QString m_path;
    Q_SIGNAL void pathChanged();

    void componentComplete() final
    {
        for (const auto &child : children()) {
            if (qobject_cast<Adaptor *>(child)) {
                KDBusPropertiesChangedAdaptor(m_path, child);
            }
        }
        QQuickItem::componentComplete();
    }

};
