// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#pragma once

#include <QQuickItem>

class BusServiceItem : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QString name MEMBER m_name NOTIFY nameChanged REQUIRED)
public:
    using QQuickItem::QQuickItem;
    QString m_name;
    Q_SIGNAL void nameChanged();
};
