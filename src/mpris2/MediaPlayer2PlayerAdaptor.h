// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#pragma once

#include "Adaptor.h"
#include <QQmlParserStatus>

// TODO temporary
#include <QMetaProperty>
#include <QDBusMessage>
#include <QDBusConnection>
#include <QDebug>
#include <QMetaObject>
#include <QEvent>

class MediaPlayer2PlayerAdaptor: public Adaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.mpris.MediaPlayer2.Player")
    Q_CLASSINFO("D-Bus Introspection",
                ""
                "  <interface name=\"org.mpris.MediaPlayer2.Player\">\n"
                "    <method name=\"Next\"/>\n"
                "    <method name=\"Previous\"/>\n"
                "    <method name=\"Pause\"/>\n"
                "    <method name=\"PlayPause\"/>\n"
                "    <method name=\"Stop\"/>\n"
                "    <method name=\"Play\"/>\n"
                "    <method name=\"Seek\">\n"
                "      <arg direction=\"in\" type=\"x\" name=\"Offset\"/>\n"
                "    </method>\n"
                "    <method name=\"SetPosition\">\n"
                "      <arg direction=\"in\" type=\"o\" name=\"TrackId\"/>\n"
                "      <arg direction=\"in\" type=\"x\" name=\"Position\"/>\n"
                "    </method>\n"
                "    <method name=\"OpenUri\">\n"
                "      <arg direction=\"in\" type=\"s\" name=\"Uri\"/>\n"
                "    </method>\n"
                "    <property access=\"read\" type=\"s\" name=\"PlaybackStatus\">\n"
                "      <annotation value=\"true\" name=\"org.freedesktop.DBus.Property.EmitsChangedSignal\"/>\n"
                "    </property>\n"
                "    <property access=\"readwrite\" type=\"s\" name=\"LoopStatus\">\n"
                "      <annotation value=\"true\" name=\"org.freedesktop.DBus.Property.EmitsChangedSignal\"/>\n"
                "    </property>\n"
                "    <property access=\"readwrite\" type=\"d\" name=\"Rate\">\n"
                "      <annotation value=\"true\" name=\"org.freedesktop.DBus.Property.EmitsChangedSignal\"/>\n"
                "    </property>\n"
                "    <property access=\"readwrite\" type=\"b\" name=\"Shuffle\">\n"
                "      <annotation value=\"true\" name=\"org.freedesktop.DBus.Property.EmitsChangedSignal\"/>\n"
                "    </property>\n"
                "    <property access=\"read\" type=\"a{sv}\" name=\"Metadata\">\n"
                "      <annotation value=\"true\" name=\"org.freedesktop.DBus.Property.EmitsChangedSignal\"/>\n"
                "      <annotation value=\"QVariantMap\" name=\"org.qtproject.QtDBus.QtTypeName\"/>\n"
                "    </property>\n"
                "    <property access=\"readwrite\" type=\"d\" name=\"Volume\">\n"
                "      <annotation value=\"true\" name=\"org.freedesktop.DBus.Property.EmitsChangedSignal\"/>\n"
                "    </property>\n"
                "    <property access=\"read\" type=\"x\" name=\"Position\">\n"
                "      <annotation value=\"false\" name=\"org.freedesktop.DBus.Property.EmitsChangedSignal\"/>\n"
                "    </property>\n"
                "    <property access=\"read\" type=\"d\" name=\"MinimumRate\">\n"
                "      <annotation value=\"true\" name=\"org.freedesktop.DBus.Property.EmitsChangedSignal\"/>\n"
                "    </property>\n"
                "    <property access=\"read\" type=\"d\" name=\"MaximumRate\">\n"
                "      <annotation value=\"true\" name=\"org.freedesktop.DBus.Property.EmitsChangedSignal\"/>\n"
                "    </property>\n"
                "    <property access=\"read\" type=\"b\" name=\"CanGoNext\">\n"
                "      <annotation value=\"true\" name=\"org.freedesktop.DBus.Property.EmitsChangedSignal\"/>\n"
                "    </property>\n"
                "    <property access=\"read\" type=\"b\" name=\"CanGoPrevious\">\n"
                "      <annotation value=\"true\" name=\"org.freedesktop.DBus.Property.EmitsChangedSignal\"/>\n"
                "    </property>\n"
                "    <property access=\"read\" type=\"b\" name=\"CanPlay\">\n"
                "      <annotation value=\"true\" name=\"org.freedesktop.DBus.Property.EmitsChangedSignal\"/>\n"
                "    </property>\n"
                "    <property access=\"read\" type=\"b\" name=\"CanPause\">\n"
                "      <annotation value=\"true\" name=\"org.freedesktop.DBus.Property.EmitsChangedSignal\"/>\n"
                "    </property>\n"
                "    <property access=\"read\" type=\"b\" name=\"CanSeek\">\n"
                "      <annotation value=\"true\" name=\"org.freedesktop.DBus.Property.EmitsChangedSignal\"/>\n"
                "    </property>\n"
                "    <property access=\"read\" type=\"b\" name=\"CanControl\">\n"
                "      <annotation value=\"false\" name=\"org.freedesktop.DBus.Property.EmitsChangedSignal\"/>\n"
                "    </property>\n"
                "    <signal name=\"Seeked\">\n"
                "      <arg type=\"x\" name=\"Position\"/>\n"
                "    </signal>\n"
                "  </interface>\n"
                "")

public:
    using Adaptor::Adaptor;

    BRIDGE_PROPERTY_READABLE(QString, PlaybackStatus, playbackStatus)
    BRIDGE_PROPERTY_WRITABLE(QString, LoopStatus, loopStatus)
    BRIDGE_PROPERTY_WRITABLE(double, Rate, rate)
    BRIDGE_PROPERTY_WRITABLE(bool, Shuffle, shuffle)
    BRIDGE_PROPERTY_READABLE(QVariantHash, Metadata, metadata)
    BRIDGE_PROPERTY_WRITABLE(double, Volume, volume)
    BRIDGE_PROPERTY_READABLE_WITHOUT_NOTIFY(qint64, Position, position)
    BRIDGE_PROPERTY_READABLE(double, MinimumRate, minimumRate)
    BRIDGE_PROPERTY_READABLE(double, MaximumRate, maximumRate)
    BRIDGE_PROPERTY_READABLE(bool, CanGoNext, canGoNext)
    BRIDGE_PROPERTY_READABLE(bool, CanGoPrevious, canGoPrevious)
    BRIDGE_PROPERTY_READABLE(bool, CanPlay, canPlay)
    BRIDGE_PROPERTY_READABLE(bool, CanPause, canPause)
    BRIDGE_PROPERTY_READABLE(bool, CanSeek, canSeek)
    BRIDGE_PROPERTY_READABLE(bool, CanControl, canControl)

    // Pretty awkward. On the qml side we deal with ints, but on the dbus side with qint64, so we coerce the former
    // into the latter by chaining signals.
    Q_SIGNAL void Seeked(qint64 Position);
    Q_SIGNAL void ppSeekedConversion(int Position);

    void afterBridging(QObject *other) override
    {
#warning this does not work in qt6 for unknown reasons
        connect(other, SIGNAL(seeked(int)), this, SIGNAL(ppSeekedConversion(int)));
        connect(this, &MediaPlayer2PlayerAdaptor::ppSeekedConversion, this, &MediaPlayer2PlayerAdaptor::Seeked);
    }

public Q_SLOTS:
    BRIDGE_CALL(void, Next, next)
    BRIDGE_CALL(void, Previous, previous)
    BRIDGE_CALL(void, Pause, pause)
    BRIDGE_CALL(void, PlayPause, playPause)
    BRIDGE_CALL(void, Stop, stop)
    BRIDGE_CALL(void, Play, play)
    void Seek(qint64 offset)
    {
        metaObject()->invokeMethod(m_other, "seek", Q_ARG(QVariant, offset));
    }
#warning todo
    // void SetPosition(const QDBusObjectPath &trackId, qint64 position)
    // {
    //     metaObject()->invokeMethod(m_other, "setPosition", Q_ARG(QVariant, trackId), Q_ARG(QVariant, position));
    // }
    void OpenUri(const QString &uri)
    {
        metaObject()->invokeMethod(m_other, "openUri", Q_ARG(QVariant, uri));
    }
};
