// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#pragma once

#include <QQuickItem>

// TODO Temporary
#include <QDBusConnection>
#include <QDBusMetaType>
#include <QDebug>
#include "Adaptor.h"
#include "BusServiceItem.h"
#include "BusItem.h"

class Bus : public QQuickItem
{
    Q_OBJECT
public:
    using QQuickItem::QQuickItem;

    void componentComplete() final
    {
        qDebug() << "bus complete";
        const auto items = childItems();
        for (const auto &item : items) {
            const auto service = qobject_cast<BusServiceItem *>(item);
            if (!service) {
                continue;
            }
            registerService(service->m_name);

            const auto objectItems = service->childItems();
            for (const auto &objectItem : objectItems) {
                const auto object = qobject_cast<BusItem *>(objectItem);
                if (!object) {
                    continue;
                }
                registerObject(object->m_path, object);
                for (const auto &adaptor : object->children()) {
                    const auto adaptorObject = qobject_cast<QDBusAbstractAdaptor *>(adaptor);
                    if (!adaptorObject) {
                        continue;
                    }
                    qDebug() << adaptorObject;
                    new KDBusPropertiesChangedAdaptor(object->m_path, adaptorObject);
                }
            }
        }
    }

public Q_SLOTS:
    bool registerService(const QString &serviceName)
    {
        return bus.registerService(serviceName);
    }

    bool registerObject(const QString &path, QObject *object)
    {
        return bus.registerObject(path, object, QDBusConnection::ExportAdaptors);
    }

private:
    QDBusConnection bus = QDBusConnection::sessionBus();
};
