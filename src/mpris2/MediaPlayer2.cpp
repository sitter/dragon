// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#include "MediaPlayer2.h"

#include "MediaPlayer2Adaptor.h"

void MediaPlayer2::componentComplete()
{
    // NB: the adaptor parent must exist and be complete by the time the adaptor constructs, this is
    // the only reason for this intermediate object. It's only here to track when qml is done so we can
    // attach our adaptor to the parent (i.e. the BusItem instance)
    (new MediaPlayer2Adaptor(parent()))->bridge(this);
}

void MediaPlayer2::classBegin()
{
}
