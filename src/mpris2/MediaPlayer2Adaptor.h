// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#pragma once

#include "Adaptor.h"
#include <QQmlParserStatus>

// TODO temporary
#include <QMetaProperty>
#include <QDBusMessage>
#include <QDBusConnection>
#include <QDebug>
#include <QMetaObject>
#include <QEvent>

class MediaPlayer2Adaptor : public Adaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.mpris.MediaPlayer2")
    Q_CLASSINFO("D-Bus Introspection",
                ""
                "  <interface name=\"org.mpris.MediaPlayer2\">\n"
                "    <annotation value=\"true\" name=\"org.freedesktop.DBus.Property.EmitsChangedSignal\"/>\n"
                "    <method name=\"Raise\"/>\n"
                "    <method name=\"Quit\"/>\n"
                "    <property access=\"read\" type=\"b\" name=\"CanQuit\"/>\n"
                "    <property access=\"readwrite\" type=\"b\" name=\"Fullscreen\"/>\n"
                "    <property access=\"read\" type=\"b\" name=\"CanSetFullscreen\"/>\n"
                "    <property access=\"read\" type=\"b\" name=\"CanRaise\"/>\n"
                "    <property access=\"read\" type=\"b\" name=\"HasTrackList\"/>\n"
                "    <property access=\"read\" type=\"s\" name=\"Identity\"/>\n"
                "    <property access=\"read\" type=\"s\" name=\"DesktopEntry\"/>\n"
                "    <property access=\"read\" type=\"as\" name=\"SupportedUriSchemes\"/>\n"
                "    <property access=\"read\" type=\"as\" name=\"SupportedMimeTypes\"/>\n"
                "  </interface>\n"
                "")
public:
    using Adaptor::Adaptor;

    BRIDGE_PROPERTY_READABLE(bool, CanQuit, canQuit)
    BRIDGE_PROPERTY_WRITABLE(bool, Fullscreen, fullscreen)
    BRIDGE_PROPERTY_READABLE(bool, CanSetFullscreen, canSetFullscreen)
    BRIDGE_PROPERTY_READABLE(bool, CanRaise, canRaise)
    BRIDGE_PROPERTY_READABLE(bool, HasTrackList, hasTrackList)
    BRIDGE_PROPERTY_READABLE(QString, Identity, identity)
    BRIDGE_PROPERTY_READABLE(QString, DesktopEntry, desktopEntry)
    BRIDGE_PROPERTY_READABLE(QStringList, SupportedUriSchemes, supportedUriSchemes)
    BRIDGE_PROPERTY_READABLE(QStringList, SupportedMimeTypes, supportedMimeTypes)

public Q_SLOTS:
    BRIDGE_CALL(void, Quit, quit)
    BRIDGE_CALL(void, Raise, raise)
};
