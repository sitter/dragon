// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#pragma once

#include <vlc/vlc.h>

#include <memory>

#include <QMutex>
#include <QOffscreenSurface>
#include <QOpenGLContext>
#include <QOpenGLFramebufferObject>
#include <QQuickWindow> // TODO could be qsurface?
#include <QSemaphore>

struct VLCWindowCallbacks {
    libvlc_video_output_resize_cb resize;
    libvlc_video_output_mouse_move_cb mouseMove;
    libvlc_video_output_mouse_press_cb mousePress;
    libvlc_video_output_mouse_release_cb mouseRelease;

    void *opaque;
};

class VideoOutput : public QObject
{
    Q_OBJECT
public:
    QOpenGLContext m_context;
    QOffscreenSurface m_surface;
    QSemaphore m_initializedSemaphore;

    QMutex m_windowCallbacksMutex;
    std::optional<VLCWindowCallbacks> m_windowCallbacks;

    unsigned m_width = 0;
    unsigned m_height = 0;
    QMutex m_fboMutex;
    std::unique_ptr<QOpenGLFramebufferObject> m_garbage; // delay garbage collection. VLC doesn't necessarily doneCurrent before changing output sizes it seems (or I am messing up - equally possible)
    std::unique_ptr<QOpenGLFramebufferObject> m_fbo;
    bool m_updated = false;
    QSize m_viewportSize;
    QBasicAtomicInt m_dead = false;

    explicit VideoOutput(QQuickWindow *window);

    void setViewportSize(const QSize &size);
    bool setup(const libvlc_video_setup_device_cfg_t *cfg, libvlc_video_setup_device_info_t *out);
    void cleanup();
    void doResize();
    void mouseMove(int x, int y);
    void mousePress(libvlc_video_output_mouse_button_t button);
    void mouseRelease(libvlc_video_output_mouse_button_t button);
    void window(libvlc_video_output_resize_cb report_size_change,
                libvlc_video_output_mouse_move_cb report_mouse_move,
                libvlc_video_output_mouse_press_cb report_mouse_pressed,
                libvlc_video_output_mouse_release_cb report_mouse_released,
                void *report_opaque);
    bool update_output(const libvlc_video_render_cfg_t *cfg, libvlc_video_output_cfg_t *output);
    void swap();
    bool makeCurrent(bool enter);
    void *getProcAddress(const char *fct_name);
    void metadata(libvlc_video_metadata_type_t type, const void *metadata);
    bool select_plane(size_t plane, void *output);
    QOpenGLFramebufferObject *getVideoFrame();

Q_SIGNALS:
    void frameReady();
};
