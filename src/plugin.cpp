// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2021 Harald Sitter <sitter@kde.org>

#include <QQmlContext>
#include <QQmlEngine>
#include <QQmlExtensionPlugin>

#include <KAboutData>
#include <KLocalizedContext>
#include <KLocalizedString>
#include <KProtocolManager>
#include <KService>

#include "dragon.h"
#include "AudioTrackModel.h"
#include "Dialog.h"
#include "MediaPlayer.h"
#include "MediaStatistics.h"
#include "SubtitleTrackModel.h"
#include "VideoItem.h"
#include "VideoOutput.h"
#include "VideoTrackModel.h"
#include "mpris2/Bus.h"
#include "mpris2/BusItem.h"
#include "mpris2/BusServiceItem.h"
#include "mpris2/MediaPlayer2.h"
#include "mpris2/MediaPlayer2Player.h"

using namespace Qt::StringLiterals;

class About : public QObject
{
    Q_OBJECT
    Q_PROPERTY(KAboutData data READ data CONSTANT)
    [[nodiscard]] static KAboutData data()
    {
        return KAboutData::applicationData();
    }

public:
    using QObject::QObject;
};

class Context : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList mimetypes MEMBER m_mimetypes CONSTANT)
    Q_PROPERTY(QStringList protocols MEMBER m_protocols CONSTANT)
public:
    using QObject::QObject;

private:
    QStringList m_mimetypes = KService::serviceByDesktopName(Dragon::desktopFileName)->mimeTypes();
    QStringList m_protocols = KProtocolInfo::protocols();
};

class ProfilesPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char *uri) override
    {
        Q_ASSERT(uri == QByteArrayLiteral("org.kde.dragon"));

        qmlRegisterSingletonType<About>(
            "org.kde.dragon", 1, 0, "About", [](QQmlEngine *, QJSEngine *) -> QObject * { return new About; });
        qmlRegisterSingletonType<Context>(
            "org.kde.dragon", 1, 0, "Context", [](QQmlEngine *, QJSEngine *) -> QObject * { return new Context; });
        qmlRegisterType<MediaPlayer>("org.kde.dragon", 1, 0, "MediaPlayer");
        qmlRegisterType<MediaStatistics>("org.kde.dragon", 1, 0, "MediaStatistics");
        qmlRegisterType<VideoItem>("org.kde.dragon", 1, 0, "VideoItem");
        qmlRegisterType<Dialog>("org.kde.dragon", 1, 0, "Dialog");
        qmlRegisterType<AudioTrackModel>("org.kde.dragon", 1, 0, "AudioTrackModel");
        qmlRegisterType<SubtitleTrackModel>("org.kde.dragon", 1, 0, "SubtitleTrackModel");
        qmlRegisterType<VideoTrackModel>("org.kde.dragon", 1, 0, "VideoTrackModel");
        qmlRegisterType<Bus>("org.kde.dragon", 1, 0, "Bus");
        qmlRegisterType<MediaPlayer2>("org.kde.dragon", 1, 0, "MediaPlayer2");
        qmlRegisterType<MediaPlayer2Player>("org.kde.dragon", 1, 0, "MediaPlayer2Player");
        qmlRegisterType<BusServiceItem>("org.kde.dragon", 1, 0, "BusService");
        qmlRegisterType<BusItem>("org.kde.dragon", 1, 0, "BusObject");

        qRegisterMetaType<intptr_t>("intptr_t");
        qRegisterMetaType<Dialog::QuestionType>("Dialog::QuestionType");
    }
};

#include "plugin.moc"
