// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#pragma once

#include <vlc/vlc.h>

#include <memory>

#include <QAbstractListModel>

#include "LibVLC.h"

// FIXME temporary
#include "Media.h"
#include <QUrl>
#include <QDebug>
#include <QMetaEnum>

class VideoOutput;

namespace std
{
template<>
struct default_delete<libvlc_media_list_t> {
    void operator()(libvlc_media_list_t *ptr) const
    {
        libvlc_media_list_release(ptr);
    }
};
} // namespace std


class MediaList : public QAbstractListModel
{
    Q_OBJECT
    struct ListLocker
    {
        explicit ListLocker(const MediaList *list)
            : m_list(list->get())
        {
            libvlc_media_list_lock(m_list);
        }
        ~ListLocker()
        {
            libvlc_media_list_unlock(m_list);
        }
        Q_DISABLE_COPY_MOVE(ListLocker);
        libvlc_media_list_t *m_list;
    };

public:
    enum class Role {
        IsCurrent = Qt::UserRole,
    };
    Q_ENUM(Role)

    explicit MediaList(QObject *parent = nullptr)
        : m_list(libvlc_media_list_new())
    {
        ListLocker lock(this);

        auto manager = libvlc_media_list_event_manager(m_list.get());
        static constexpr auto events = {
            libvlc_MediaListItemAdded,
            libvlc_MediaListWillAddItem,
            libvlc_MediaListItemDeleted,
            libvlc_MediaListWillDeleteItem,
            libvlc_MediaListEndReached,
        };

        for (const auto &event : events) {
            libvlc_event_attach(manager, event, event_cb, this);
        }
    }

    void add(Media *media)
    {
        ListLocker lock(this);
        libvlc_media_list_add_media(m_list.get(), media->get());
    }

    [[nodiscard]] bool remove(int index)
    {
        ListLocker lock(this);
        return 0 == libvlc_media_list_remove_index(m_list.get(), index);
    }

    [[nodiscard]] std::unique_ptr<Media> at(int index)
    {
        ListLocker lock(this);
        return std::make_unique<Media>(libvlc_media_list_item_at_index(m_list.get(), index), Media::Retain::No);
    }

    [[nodiscard]] int find(Media *media)
    {
        ListLocker lock(this);
        return libvlc_media_list_index_of_item(m_list.get(), media->get());
    }

    [[nodiscard]] int count() const
    {
        ListLocker lock(this);
        return libvlc_media_list_count(get());
    }

    [[nodiscard]] int rowCount(const QModelIndex &parent) const override
    {
        Q_UNUSED(parent);
        return count();
    }

    [[nodiscard]] QVariant data(const QModelIndex &index, int intRole) const override
    {
        if (!index.isValid()) {
            return {};
        }

        ListLocker lock(this);
        Media media(libvlc_media_list_item_at_index(get(), index.row()));
        switch (intRole) {
        case Qt::DisplayRole:
            return media.mrl();
        }

        switch (Role(intRole)) {
        case Role::IsCurrent:
            return m_currentMedia ? m_currentMedia->get() == media.get() : false;
        }

        return {};
    }

    void setCurrentMedia(std::unique_ptr<Media> media)
    {
        auto previousMedia = std::move(m_currentMedia);
        m_currentMedia = std::move(media);

        // notify both entries AFTER the change has been executed otherwise we'd end up with queries mid-flight

        auto notifyCurrentChange = [this](Media *media) {
            const auto row = find(media);
            Q_ASSERT(row >= 0);
            Q_EMIT dataChanged(index(row, 0), index(row, 0), {int(Role::IsCurrent)});
        };

        if (previousMedia) {
            notifyCurrentChange(previousMedia.get());
        }
        notifyCurrentChange(m_currentMedia.get());
    }

    QHash<int, QByteArray> roleNames() const override
    {
        static QHash<int, QByteArray> roles;
        if (!roles.isEmpty()) {
            return roles;
        }

        roles = QAbstractListModel::roleNames();
        const QMetaEnum roleEnum = QMetaEnum::fromType<Role>();
        for (int i = 0; i < roleEnum.keyCount(); ++i) {
            const int value = roleEnum.value(i);
            Q_ASSERT(value != -1);
            roles[static_cast<int>(value)] = QByteArray("ROLE_") + roleEnum.valueToKey(value);
        }
        return roles;
    }

    libvlc_media_list_t *get() const
    {
        return m_list.get();
    }

private:
    static void event_cb(const libvlc_event_t *event, void *opaque)
    {
        auto that = static_cast<MediaList *>(opaque);
        Q_ASSERT(that);

        // Do not forget to register for the events you want to handle here!
        // NB: queue connections here or you might end up in a call chain where the mutex is already locked!
        switch (event->type) {
        case libvlc_MediaListItemAdded:
            QMetaObject::invokeMethod(that, [that] {
                that->endInsertRows();
            }, Qt::QueuedConnection);
            break;
        case libvlc_MediaListWillAddItem: {
            const auto index = event->u.media_list_will_add_item.index;
            QMetaObject::invokeMethod(that, [index, that] {
                that->beginInsertRows({}, index, index);
            }, Qt::QueuedConnection);
            break;
        }
        case libvlc_MediaListItemDeleted:
            QMetaObject::invokeMethod(that, [that] {
                that->endRemoveRows();
            }, Qt::QueuedConnection);
            break;
        case libvlc_MediaListWillDeleteItem:{
            const auto index = event->u.media_list_will_delete_item.index;
            QMetaObject::invokeMethod(that, [index, that] {
                that->beginRemoveRows({}, index, index);
            }, Qt::QueuedConnection);
            break;
        }
        case libvlc_MediaListEndReached:
            qDebug() << "media list end reached";
            break;
        }
    }

    std::unique_ptr<Media> m_currentMedia;
    std::unique_ptr<libvlc_media_list_t> m_list;
};

