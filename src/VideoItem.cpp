// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#include "VideoItem.h"

#include <QBrush>
#include <QSGSimpleTextureNode>
#include <QSGTexture>

#include "VideoOutput.h"

class VideoRenderer : public QObject
{
    Q_OBJECT
public:
    explicit VideoRenderer(QQuickItem *item)
        : m_item(item)
    {
    }

    void setViewportSize(const QSize &size)
    {
        m_viewportSize = size;
        if (m_vout) {
            m_vout->setViewportSize(size);
        }
    }
    void setWindow(QQuickWindow *window)
    {
        m_window = window;
    }

    QSGTexture *texture() const
    {
        QOpenGLFramebufferObject const* fbo = m_vout->getVideoFrame();
        if (!fbo) {
            return nullptr;
        }

        const auto handle = fbo->texture();
        return QNativeInterface::QSGOpenGLTexture::fromNative(handle, m_window, m_viewportSize, {});
    }

public:
    VideoOutput *m_vout = nullptr;
    QQuickItem const *m_item;
    QSize m_viewportSize;
    qreal m_t = 0.0;
    QQuickWindow *m_window = nullptr;
};

class TextureNode : public QObject, public QSGSimpleTextureNode
{
    Q_OBJECT
public:
    explicit TextureNode(QQuickWindow *window)
    {
        // Our texture node must have a texture, so use the default 0 texture.
        const GLuint handle = 0;
        const QSize viewportSize(1, 1);
        auto texture = QNativeInterface::QSGOpenGLTexture::fromNative(handle, window, viewportSize, {});
        setTexture(texture);
        setFiltering(QSGTexture::Linear);
    }
};

VideoItem::VideoItem()
{
    setFlag(ItemHasContents);
    connect(this, &QQuickItem::windowChanged, this, &VideoItem::handleWindowChanged);
}

VideoItem::~VideoItem() = default;

void VideoItem::ensureRenderer()
{
    if (!m_renderer) {
        m_vout = new VideoOutput(window());
        m_renderer = std::make_unique<VideoRenderer>(this);
        m_renderer->m_vout = m_vout;

        m_renderer->setWindow(window());
    }
}

QSGNode *VideoItem::updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *)
{
    auto node = dynamic_cast<TextureNode *>(oldNode);

    if (!node) {
        node = new TextureNode(window());
        node->setTextureCoordinatesTransform(QSGSimpleTextureNode::MirrorVertically);
        connect(
            window(),
            &QQuickWindow::beforeRendering,
            node,
            [node, this] {
                if (auto texture = m_renderer->texture()) {
                    node->setTexture(texture);
                } else {
                    // When the render has no texture (yet), set a static black texture instead
                    node->setTexture(window()->createTextureFromImage(QBrush(Qt::black).textureImage()));
                }

            },
            Qt::DirectConnection);
    }

    node->setRect(boundingRect());

    return node;
}

void VideoItem::sync()
{
    ensureRenderer();
    m_renderer->setViewportSize((size() * window()->devicePixelRatio()).toSize());
    m_renderer->setWindow(window());
}

void VideoItem::cleanup()
{
    m_renderer = nullptr;
}

void VideoItem::handleWindowChanged(QQuickWindow *win)
{
    ensureRenderer();
    if (win) {
        connect(win, &QQuickWindow::beforeSynchronizing, this, &VideoItem::sync, Qt::DirectConnection);
        connect(win, &QQuickWindow::sceneGraphInvalidated, this, &VideoItem::cleanup, Qt::DirectConnection);
        win->setColor(Qt::black);
    }
}

void VideoItem::releaseResources()
{
}

#include "VideoItem.moc"
