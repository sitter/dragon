# SPDX-License-Identifier: BSD-3-Clause
# SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

cmake_minimum_required(VERSION 3.22)

# KDE Application Version, managed by release script
set(RELEASE_SERVICE_VERSION_MAJOR "23")
set(RELEASE_SERVICE_VERSION_MINOR "03")
set(RELEASE_SERVICE_VERSION_MICRO "70")
set(RELEASE_SERVICE_VERSION "${RELEASE_SERVICE_VERSION_MAJOR}.${RELEASE_SERVICE_VERSION_MINOR}.${RELEASE_SERVICE_VERSION_MICRO}")

project(DragonPlayer VERSION ${RELEASE_SERVICE_VERSION})

set(PROJECT_VERSION ${RELEASE_SERVICE_VERSION})

set(QT_MIN_VERSION "5.15.2")
set(KF5_MIN_VERSION "5.240.0")

option(FORCE_VLC "Force use of libvlc regardless of pkgconfig" OFF)

find_package(ECM ${KF5_MIN_VERSION} REQUIRED NO_MODULE)
set(CMAKE_MODULE_PATH ${ECM_MODULE_PATH} ${ECM_KDE_MODULE_DIR})

include(FeatureSummary)
include(KDEInstallDirs)
include(KDECMakeSettings)
include(KDECompilerSettings)
include(FindPkgConfig)
include(ECMInstallIcons)
include(ECMQmlModule)

set(CMAKE_CXX_STANDARD 23)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# Disable legacy stuff to get rid of some deprecation warnings.
add_definitions(-DQT_DISABLE_DEPRECATED_BEFORE=0x060600)

find_package(Qt${QT_MAJOR_VERSION} 5.15 CONFIG REQUIRED Core DBus Qml Quick Widgets)
find_package(KF6 ${KF5_MIN_VERSION} REQUIRED
    I18n
    KIO
    Service
)
if(FORCE_VLC)
    find_library(lib NAMES vlc)
    find_path(include vlc/libvlc.h)
    add_library(PkgConfig::libvlc SHARED IMPORTED)
    set_target_properties(PkgConfig::libvlc PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES ${include}
        IMPORTED_LOCATION ${lib}
    )
else()
    pkg_check_modules(libvlc REQUIRED IMPORTED_TARGET libvlc)
endif()

add_definitions(-DTRANSLATION_DOMAIN="dragonplayer")

add_subdirectory(src)

install(PROGRAMS org.kde.dragonplayer.desktop DESTINATION ${KDE_INSTALL_APPDIR})
install(FILES org.kde.dragonplayer.appdata.xml DESTINATION ${KDE_INSTALL_METAINFODIR})
install(FILES dragonplayer.svg DESTINATION ${KDE_INSTALL_FULL_ICONDIR}/hicolor/scalable/apps)

ki18n_install(po)

feature_summary(WHAT ALL FATAL_ON_MISSING_REQUIRED_PACKAGES)
